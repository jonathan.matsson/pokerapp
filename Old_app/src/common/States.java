package common;

/**
 * Enum of different States in a pokergame
 */
public enum States
{
    WAITING,
    GO,
    SMALL,
    BIG,
    FOLD,
    CHECK,
    CALL,
    RAISE,
    ALL_IN
}
