package yabs.training.client;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import yabs.training.common.Card;
import yabs.training.common.GameState;
import yabs.training.common.Hand;
import yabs.training.common.Log;
import yabs.training.common.Player;
import yabs.training.common.communication.CommunicationProtocol;
import yabs.training.common.communication.GameStateMessage;
import yabs.training.common.communication.Message;
import yabs.training.common.communication.MessageConstants;
import yabs.training.common.communication.MessageId;

public class GameSession implements Runnable {
    private final ClientConnection connection;
    private final BlockingQueue<Message> gameSessionQueue;

    private GameState gameState;
    private String userId;

    /**
     * Stores the last timestamp that was sent in a GameStateRequest message.
     * This timestamp is used as a unique identifier, the response from the server
     * should contain the same timestamp.
     */
    private long latestGameStateTimestamp;

    /**
     * Stores the last timestamp that was received in a ActionRequest message.
     * This timestamp is used as a unique identifier, the response from this client
     * should contain the same timestamp.
     */
    private long latestActionTimestamp;

    public GameSession(ClientConnection connection, BlockingQueue<Message> gameSessionQueue) {
        this.connection = connection;
        this.gameSessionQueue = gameSessionQueue;

        this.gameState = null;
        this.userId = null;
    }

    @Override
    public void run() {
        try {

            this.connect();
            while (true) {
                Message msg = this.gameSessionQueue.take();
                Log.debug("GameSession received msg: " + msg.getId().name());
                this.handleMessage(msg);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public GameState getGameState() {
        return this.gameState;
    }

    public long getLatestGameStateTimestamp() {
        return this.latestGameStateTimestamp;
    }

    public long getLatestActionTimestamp() {
        return this.latestActionTimestamp;
    }

    public void setLatestGameStateTimestamp(long latestTimestamp) {
        this.latestGameStateTimestamp = latestTimestamp;
    }

    public void setLatestActionTimestamp(long latestTimestamp) {
        this.latestActionTimestamp = latestTimestamp;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Handles the initial communication between this client and the server.
     * 
     * There are three message sent between the client and the server during
     * the initial handshake:
     *   1. When the server is ready to communicate with the client, it will send
     *      a "Connected" message containing the clients user ID.
     *   2. The client will follow up with a "GameStateRequest" to get the current
     *      state of the game that it is now connected to.
     *   3. The server will respond with a "GameStateResponse".
     */
    protected void connect() throws InterruptedException, IOException {
        // Need to wait for a `Connected` message from the server until this
        // client is allowed to start doing things.
        Message msg;
        while (true) {
            msg = this.gameSessionQueue.take();
            if (msg.getId() == MessageId.Connected) {
                break;
            }
        }
        this.handleConnected(msg);

        this.setLatestGameStateTimestamp(System.currentTimeMillis());
        Message gameStateRequest = CommunicationProtocol.gameStateRequest(this.getLatestGameStateTimestamp());
        this.connection.send(gameStateRequest);

        // Need to wait for a `GameStateResponse` message from the server until
        // this client is allowed to start doing things.
        while (true) {
            msg = this.gameSessionQueue.take();
            if (msg.getId() == MessageId.GameStateResponse) {
                break;
            }
        }
        this.handleGameStateResponse(msg);
    }

    protected void handleMessage(Message msg) throws IOException {
        switch (msg.getId()) {
            /* Messages that will be received from the server. */
            case GameStateResponse:
                this.handleGameStateResponse(msg);
                break;
            case GameStateUpdateBet:
                this.handleGameStateUpdateBet(msg);
                break;
            case GameStateUpdateFold:
                this.handleGameStateUpdateFold(msg);
                break;
            case GameStateUpdateCheck:
                this.handleGameStateUpdateCheck(msg);
                break;
            case GameStateUpdateHandOutcome:
                this.handleGameStateUpdateHandOutcome(msg);
                break;
            case GameStateUpdateJoin:
                this.handleGameStateUpdateJoin(msg);
                break;
            case GameStateUpdateLeave:
                this.handleGameStateUpdateLeave(msg);
                break;
            case GameStateUpdateNewCard:
                this.handleGameStateUpdateNewCard(msg);
                break;
            case GameStateUpdateNewHand:
                this.handleGameStateUpdateNewHand(msg);
                break;
            case ActionRequest:
                this.handleActionRequest(msg);
                break;
            case TextMessageBroadCast:
                this.handleTextMessageBroadCast(msg);
                break;
            case Error:
                this.handleError(msg);
                break;

            /* Messages that will be received from user input. */
            case GameStateRequest:
                this.handleGameStateRequest(msg);
                break;
            case ActionResponseBet:
                this.handleActionResponseBet(msg);
                break;
            case ActionResponseCheck:
                this.handleActionResponseCheck(msg);
                break;
            case ActionResponseFold:
                this.handleActionResponseFold(msg);
                break;
            case TextMessage:
                this.handleTextMessage(msg);
                break;

            /* The client should never get these messages at this point. */
            case Connected:
            case Disconnect:
            default:
                String errMsg = "Received unexpected message ID: " + msg.getId();
                throw new IllegalStateException(errMsg);

        }
    }

    private void handleConnected(Message msg) {
        msg.getId().assertEquals(MessageId.Connected);

        this.userId = msg.getString(MessageConstants.USER_ID);
        Log.debug("Received a new Connected call");
    }

    private void handleGameStateResponse(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateResponse);

        long responseTimeStamp = msg.getLong(MessageConstants.TIMESTAMP);
        if (responseTimeStamp != this.getLatestGameStateTimestamp()) {
            String errMsg = "Received incorrect timestamp in \"GameStateResponse\" msg. ";
            errMsg += "Expected: " + this.getLatestGameStateTimestamp() +  ", got: " + responseTimeStamp;
            throw new IllegalStateException(errMsg);
        }

        this.gameState = GameSession.getGameState(msg);
        Log.debug("Received a new GameStateResponse");

        this.displayGameState();
    }

    private void handleGameStateUpdateBet(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateBet);

        String userId = msg.getString(MessageConstants.USER_ID);
        int amount = msg.getInt(MessageConstants.AMOUNT);

        this.handleGameStateDeduct(userId, amount);
        Log.info("Player with ID \"" + userId+ "\" betted " + amount);
    }

    private void handleGameStateUpdateFold(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateFold);

        String userId = msg.getString(MessageConstants.USER_ID);
        this.gameState.foldPlayer(userId);

        Log.info("Player with ID \"" + userId + "\" folded");
    }

    private void handleGameStateUpdateCheck(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateCheck);

        String userId = msg.getString(MessageConstants.USER_ID);
        Log.info("Player with ID \"" + userId + "\" checked");
    }

    private void handleGameStateUpdateHandOutcome(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateHandOutcome);

        String userId = msg.getString(MessageConstants.USER_ID);
        Hand hand = msg.getHand();
        int pot = this.gameState.getPot();

        Player player = this.gameState.getPlayer((String)userId);
        if (player == null) {
            String errMsg = "Unable to find player with userId \"" + (String)userId + "\"";
            throw new IllegalStateException(errMsg);
        }

        player.addToWallet(pot);
        this.gameState.reset();

        Log.info("Player with ID \"" + userId + "\" wins " + pot + " with the hand: " + hand);

        if (this.userId != null) {
            Player thisPlayer = this.gameState.getPlayer(this.userId);
            int thisWallet = thisPlayer.getWallet();
            Log.info("My wallet after this round: " + thisWallet + "\n");
        }
    }

    private void handleGameStateUpdateJoin(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateJoin);

        String userId = msg.getString(MessageConstants.USER_ID);
        int wallet = msg.getInt(MessageConstants.WALLET);
        int position = msg.getInt(MessageConstants.POSITION);

        Player player = new Player(userId, wallet);
        this.gameState.addPlayer(position, player);

        Log.debug("Player with ID \"" + userId + "\" with wallet " + wallet + " added at position " + position);
    }

    private void handleGameStateUpdateLeave(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateLeave);

        String userId = msg.getString(MessageConstants.USER_ID);
        this.gameState.removePlayer(userId);

        Log.debug("Player with ID \"" + userId + "\" removed");
    }

    private void handleGameStateUpdateNewCard(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateNewCard);

        Object cardObj = msg.get(MessageConstants.CARD);
        if (!(cardObj instanceof Card)) {
            String errMsg = "GameStateUpdateNewCard didn't contain card: \"" + cardObj + "\"";
            throw new IllegalStateException(errMsg);
        }

        Card card = (Card)cardObj;
        this.gameState.addCommunityCard(card);

        Log.info("New card added to GameState: " + card);
    }

    private void handleGameStateUpdateNewHand(Message msg) {
        msg.getId().assertEquals(MessageId.GameStateUpdateNewHand);

        Object handObj = msg.get(MessageConstants.HAND);
        if (!(handObj instanceof Hand)) {
            String errMsg = "GameStateUpdateNewHand didn't contain hand: \"" + handObj + "\"";
            throw new IllegalStateException(errMsg);
        }

        Hand hand = (Hand)handObj;
        Player player = this.gameState.getPlayer(this.userId);
        player.setHand(hand);

        Log.info("New hand delt to this client: " + hand);
    }


    private void handleActionRequest(Message msg) {
        msg.getId().assertEquals(MessageId.ActionRequest);

        this.setLatestActionTimestamp(msg.getLong(MessageConstants.TIMESTAMP));

        // TODO: Make a GameStateRequest if the current GameState is null.
        if (this.gameState != null) {
            this.displayGameState();
        }

        Log.info("Got ActionRequest -- b[et], c[heck] or f[old]");
    }

    private void handleTextMessageBroadCast(Message msg) {
        msg.getId().assertEquals(MessageId.TextMessageBroadCast);

        String userId = msg.getString(MessageConstants.USER_ID);
        String chatMsg = msg.getString(MessageConstants.MESSAGE);

        Log.info("Player with ID \"" + userId + "\" wrote: " + chatMsg);
    }

    private void handleGameStateDeduct(String userId, int amount) {
        Player player = this.gameState.getPlayer((String)userId);
        if (player == null) {
            String errMsg = "Unable to find player with userId \"" + (String)userId + "\"";
            throw new IllegalStateException(errMsg);
        }

        // If a player is trying to bet more than they have,
        // set the actual bet amount equal to their wallet
        if(amount > player.getWallet()) {
            amount = player.getWallet();
        }

        if (amount < 0) {
            String errMsg = "Tried to deduct a negative value from player \"" + userId + "\"'s wallet.";
            errMsg += " Current wallet: " + player.getWallet() + ", tried to deduct: " + amount;
            throw new IllegalStateException(errMsg);
        }

        player.deductFromWallet(amount);
        this.gameState.increasePot(amount);
        this.gameState.raisePlayerBet(userId, amount);
    }

    private void handleGameStateRequest(Message msg) throws IOException {
        msg.getId().assertEquals(MessageId.GameStateRequest);

        this.setLatestGameStateTimestamp(msg.getLong(MessageConstants.TIMESTAMP));
        this.connection.send(msg);

        Log.debug("Sent a new GameStateRequest");
    }

    private void handleActionResponseBet(Message msg) throws IOException {
        msg.getId().assertEquals(MessageId.ActionResponseBet);
        msg.set(MessageConstants.TIMESTAMP, this.getLatestActionTimestamp());

        int amount = msg.getInt(MessageConstants.AMOUNT);
        this.handleGameStateDeduct(this.userId, amount);

        this.connection.send(msg);
        Log.debug("Sent ActionResponseBet");
    }

    private void handleActionResponseCheck(Message msg) throws IOException {
        msg.getId().assertEquals(MessageId.ActionResponseCheck);
        msg.set(MessageConstants.TIMESTAMP, this.getLatestActionTimestamp());

        this.connection.send(msg);
        Log.debug("Sent ActionResponseCheck");
    }

    private void handleActionResponseFold(Message msg) throws IOException {
        msg.getId().assertEquals(MessageId.ActionResponseFold);
        msg.set(MessageConstants.TIMESTAMP, this.getLatestActionTimestamp());

        this.gameState.foldPlayer(this.userId);

        this.connection.send(msg);
        Log.debug("Sent ActionResponseFold");
    }

    private void handleTextMessage(Message msg) throws IOException {
        msg.getId().assertEquals(MessageId.TextMessage);

        this.connection.send(msg);
        Log.debug("Sent TextMessage");
    }

    private void handleError(Message msg) throws IOException {
        msg.getId().assertEquals(MessageId.Error);

        String errMsg = msg.getString(MessageConstants.MESSAGE);
        long errTimestamp = msg.getLong(MessageConstants.TIMESTAMP);

        Log.error("Received error: " + errMsg + "\nError timestamp:" + errTimestamp);
    }

    private static GameState getGameState(Message msg) {
        if (!(msg instanceof GameStateMessage)) {
            String errMsg = "Tried to get gameState from a non GameStateMessage message.";
            throw new IllegalStateException(errMsg);
        }
        return ((GameStateMessage)msg).getGameState();
    }

    private void displayGameState() {
        StringBuilder sb = new StringBuilder(this.gameState.toString());

        Player player = this.gameState.getPlayer(this.userId);
        if (player == null) {
            sb.append("\n  Unable to find player with user ID \"" + this.userId + "\"");
            Log.error(sb.toString());
            return;
        }

        sb.append("\n\n  My user id: \"" + this.userId + "\"");
        sb.append("\n  Cards in hand: " + player.getHand());
        sb.append("\n  My wallet: " + player.getWallet());
        sb.append("\n  My current bet: " + this.gameState.getPlayerBet(this.userId));
        sb.append("\n  Highest bet (need to match amount): " + this.gameState.getHighestLiveBet());

        Log.info(sb.toString());
    }
}
