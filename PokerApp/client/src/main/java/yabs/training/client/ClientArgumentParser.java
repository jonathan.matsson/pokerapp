package yabs.training.client;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import yabs.training.common.ArgumentParser;

public class ClientArgumentParser {
    public static CommandLine parse(String[] args) {
        Options options = new Options();
        Option option;

        option = new Option("a", ArgumentParser.SERVER_ADDRESS, true, "The hostname/IP of the server.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("p", ArgumentParser.SERVER_PORT, true, "The port of the server.");
        option.setRequired(false);
        options.addOption(option);

        return ArgumentParser.parse(options, args, "PokerApp Client");
    }
}
