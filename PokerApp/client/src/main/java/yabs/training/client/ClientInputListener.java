package yabs.training.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;

import yabs.training.common.Log;
import yabs.training.common.communication.CommunicationProtocol;
import yabs.training.common.communication.Message;

/**
 * A class that will listen for user input by the user on standard in. Depending
 * on the input it will create messages that will be put into the `gameSessionQueue`
 * which should be read from the GameSession that in turn sends it to the server.
 */
public class ClientInputListener implements Runnable {
    private final BlockingQueue<Message> gameSessionQueue;

    public ClientInputListener(BlockingQueue<Message> gameSessionQueue) {
        this.gameSessionQueue = gameSessionQueue;
    }

    @Override
    public void run() {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
            while(true) {

                String line = in.readLine();
                String lineLC = line.toLowerCase();

                if (lineLC.startsWith("q") || lineLC.startsWith("e")) { // "q[uit] / e[xit]"
                    // TODO: Need to notify "main" in some way about this in a
                    //       better way than just calling "exit()". So that the
                    //       program can be shut down gracefully.
                    System.exit(0);
                } else if (lineLC.startsWith("req") || lineLC.startsWith("gamestate") || lineLC.startsWith("u")) {
                    this.handleGameStateRequest();
                } else if (lineLC.startsWith("b")) {  // "b[et] <AMOUNT>"
                    this.handleBet(line, lineLC);
                } else if (lineLC.equals("c") || lineLC.equals("check")) {  // "c[heck]"
                    this.handleCheck();
                } else if (lineLC.equals("f") || lineLC.equals("fold")) {  // "f[old]"
                    this.handleFold();
                } else if (lineLC.startsWith("t")) {  // "t[ext] <TEXT>..."
                    this.handleText(line);
                } else {
                    String helpMsg = "Unrecognized command: \"" + line + "\"\nValid commands:";
                    helpMsg += "\n - q[uit]";
                    helpMsg += "\n - gamestate (to fetch the latest GameState from the server)";
                    helpMsg += "\n - b[et] <AMOUNT>";
                    helpMsg += "\n - c[heck]";
                    helpMsg += "\n - f[old]";
                    helpMsg += "\n - t[ext] <TEXT>";
                    Log.error(helpMsg);
                }

            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void handleGameStateRequest() throws InterruptedException {
        Message msg = CommunicationProtocol.gameStateRequest(System.currentTimeMillis());
        this.gameSessionQueue.put(msg);
    }

    private void handleBet(String line, String lineLC) throws InterruptedException {
        String[] lineParts = lineLC.split(" ");

        if (lineParts.length != 2 || !(lineParts[0].equals("b") || lineParts[0].equals("bet"))) {
            String errMsg = "Invalid bet command: " + line;
            errMsg += "\nExpected format: \"bet <AMOUNT>\"";
            Log.error(errMsg);
            return;
        }

        int amount;
        try {
            amount = Integer.valueOf(lineParts[1]);
        } catch (NumberFormatException e) {
            String errMsg = "Invalid amount in bet command: " + line;
            errMsg += "\nExpected bet amount to be integer.";
            Log.error(errMsg);
            return;
        }

        if (amount < 0) {
            String errMsg = "Tried to bet a negative amount, not allowed: " + amount;
            Log.error(errMsg);
            return;
        }

        // OBS! The timestamp isn't known here, it will be set by the GameSession.
        Message msg = CommunicationProtocol.actionResponseBet(-1, amount);
        this.gameSessionQueue.put(msg);
    }

    private void handleCheck() throws InterruptedException {
        // OBS! The timestamp isn't known here, it will be set by the GameSession.
        Message msg = CommunicationProtocol.actionResponseCheck(-1);
        this.gameSessionQueue.put(msg);
    }

    private void handleFold() throws InterruptedException {
        // OBS! The timestamp isn't known here, it will be set by the GameSession.
        Message msg = CommunicationProtocol.actionResponseFold(-1);
        this.gameSessionQueue.put(msg);
    }

    private void handleText(String line) throws InterruptedException {
        int firstSpaceIdx = line.indexOf(" ");
        if (firstSpaceIdx < 0 || firstSpaceIdx >= line.length() - 1) {
            String errMsg = "Invalid text command: " + line;
            errMsg += "\nExpected format: \"text <TEXT>...\"";
            Log.error(errMsg);
            return;
        }

        String textMsg = line.substring(firstSpaceIdx + 1);

        Message msg = CommunicationProtocol.textMessage(textMsg);
        this.gameSessionQueue.put(msg);
    }
}
