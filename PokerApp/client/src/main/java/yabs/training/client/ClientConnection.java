package yabs.training.client;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

import yabs.training.common.communication.CommunicationProtocol;
import yabs.training.common.communication.Message;

/**
 * This class will handle all communication between this client and a server.
 * 
 * The messages received from the server will be put into the `gameSessionQueue`
 * so that they can be read by the GameSession.
 * 
 * The thread running the GameSession will call the `send()` function to send
 * messages to the server, the message will NOT be sent on the thread that runs
 * this ClientConnection (should this be the case?).
 */
public class ClientConnection implements AutoCloseable, Runnable {
    /**
     * Queue used to communicate with the GameSession. Any message that this
     * ClientConnection gets from the server, it will put into this queue which
     * will read by the GameSession. 
     */
    private final BlockingQueue<Message> gameSessionQueue;

    /**
     * The socket connection between this client and a server.
     */
    private final Socket socket;

    public ClientConnection(BlockingQueue<Message> gameSessionQueue, Socket socket) {
        this.gameSessionQueue = gameSessionQueue;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {

            while (true) {
                Message msg = CommunicationProtocol.readMessage(this.socket.getInputStream());
                this.gameSessionQueue.put(msg);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void close() throws Exception {
        if (this.socket != null) {
            this.socket.close();;
        }
    }

    /**
     * Sends a message from this client to the server.
     * 
     * @param msg the Message to send.
     * @throws IOException if unable to send message.
     */
    public void send(Message msg) throws IOException {
        CommunicationProtocol.sendMessage(this.socket.getOutputStream(), msg);
    }
}
