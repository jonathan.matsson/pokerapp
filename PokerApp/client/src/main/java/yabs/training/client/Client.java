package yabs.training.client;

import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.cli.CommandLine;

import yabs.training.common.ArgumentParser;
import yabs.training.common.communication.Message;

// TODO: Get server address and port as arguments.

public class Client {
    private static final int DEFAULT_SERVER_PORT = 4999;

    public static void main(String[] args) throws Exception {
        CommandLine cmdOptions = ClientArgumentParser.parse(args);

        // If the `SERVER_ADDRESS` option isn't set, it defaults to `localhost`.
        InetAddress serverAddress = ArgumentParser.getInetAddressOption(cmdOptions, ArgumentParser.SERVER_ADDRESS);
        int serverPort = ArgumentParser.getIntOption(cmdOptions, ArgumentParser.SERVER_PORT, DEFAULT_SERVER_PORT);

        List<Thread> threads = new ArrayList<>(3);

        // Queue/channel used to communicate between the threads.
        BlockingQueue<Message> gameSessionQueue = new LinkedBlockingQueue<>();

        ClientInputListener inputListener = new ClientInputListener(gameSessionQueue);
        threads.add(new Thread(inputListener));

        Socket socket = new Socket(serverAddress, serverPort);
        ClientConnection connection = new ClientConnection(gameSessionQueue, socket);
        threads.add(new Thread(connection));

        GameSession gameSession = new GameSession(connection, gameSessionQueue);
        threads.add(new Thread(gameSession));

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
    }
}
