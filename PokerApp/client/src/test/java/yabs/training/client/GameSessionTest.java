package yabs.training.client;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import yabs.training.common.Card;
import yabs.training.common.GameState;
import yabs.training.common.Hand;
import yabs.training.common.Player;
import yabs.training.common.Rank;
import yabs.training.common.Suit;
import yabs.training.common.communication.GameStateMessage;
import yabs.training.common.communication.Message;
import yabs.training.common.communication.MessageConstants;
import yabs.training.common.communication.MessageId;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class GameSessionTest {
    @Test
    public void handleGameStateResponseCorrectly() throws Exception {
        // ARRANGE
        int expectedMaxPlayerNumber = 0;
        int expectedSmallBlindAmount = 1;
        int expectedBigBlindAmount = 2;
        GameState expectedGameState = new GameState(expectedMaxPlayerNumber, expectedSmallBlindAmount, expectedBigBlindAmount);
        long expectedTimestamp = 123;

        Message msg = new GameStateMessage(MessageId.GameStateResponse, expectedGameState);
        msg.set(MessageConstants.TIMESTAMP, Long.toString(expectedTimestamp));

        GameSession gameSession = new GameSession(null, null);
        gameSession.setLatestGameStateTimestamp(expectedTimestamp);

        // ACT
        gameSession.handleMessage(msg);
        GameState actualGameState = gameSession.getGameState();

        // ASSERT
        assertEquals(expectedMaxPlayerNumber, actualGameState.getMaxPlayerNumber());
        assertEquals(expectedSmallBlindAmount, actualGameState.getSmallBlindAmount());
        assertEquals(expectedBigBlindAmount, actualGameState.getBigBlindAmount());
    }

    @Test
    public void handleGameStateResponseThrowsIfIncorrectTimestamp() {
        // ARRANGE
        GameState expectedGameState = new GameState(0, 1, 2);
        long correctTimestamp = 123;
        long incorrectTimestamp = 456;

        Message msg = new GameStateMessage(MessageId.GameStateResponse, expectedGameState);
        msg.set(MessageConstants.TIMESTAMP, Long.toString(incorrectTimestamp));

        GameSession gameSession = new GameSession(null, null);
        gameSession.setLatestGameStateTimestamp(correctTimestamp);

        // ACT & ASSERT
        assertThrows(IllegalStateException.class, () -> gameSession.handleMessage(msg));
    }

    @Test
    public void handleGameStateUpdateBetCorrectly() throws Exception {
        // ARRANGE
        String expectedUserId = "USER_ID";
        int expectedBetAmount = 1;
        int playerWallet = 100;

        Player player = Mockito.mock(Player.class);
        Mockito.when( player.getWallet() ).thenReturn( playerWallet );

        GameState gameState = Mockito.mock(GameState.class);
        Mockito.when( gameState.getPlayer(eq(expectedUserId)) ).thenReturn( player );

        Message msg = new Message(MessageId.GameStateUpdateBet);
        msg.set(MessageConstants.USER_ID, expectedUserId);
        msg.set(MessageConstants.AMOUNT, Integer.toString(expectedBetAmount));

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(gameState);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(gameState, times(1)).raisePlayerBet(anyString(), eq(expectedBetAmount));
        verify(gameState, times(1)).increasePot(eq(expectedBetAmount));
    }

    @Test
    public void handleGameStateUpdateBetThrowsIfIncorrectUserId() {
        // ARRANGE
        String incorrectUserId = "USER_ID";
        int expectedAmount = 1;

        GameState gameState = Mockito.mock(GameState.class);
        Mockito.when( gameState.getPlayer(eq(incorrectUserId)) ).thenReturn( null );

        Message msg = new Message(MessageId.GameStateUpdateBet);
        msg.set(MessageConstants.USER_ID, incorrectUserId);
        msg.set(MessageConstants.AMOUNT, Integer.toString(expectedAmount));

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(gameState);

        // ACT & ASSERT
        assertThrows(IllegalStateException.class, () -> gameSession.handleMessage(msg));
    }

    @Test
    public void handleGameStateUpdateBetDoesNotBetWithOverflow() throws Exception {
        // ARRANGE
        String expectedUserId = "USER_ID";
        int playerWallet = 99;
        int expectedBetAmount = playerWallet;
        int betAmount = 100;

        Player player = Mockito.mock(Player.class);
        Mockito.when( player.getWallet() ).thenReturn( playerWallet );

        GameState gameState = Mockito.mock(GameState.class);
        Mockito.when( gameState.getPlayer(eq(expectedUserId)) ).thenReturn( player );

        Message msg = new Message(MessageId.GameStateUpdateBet);
        msg.set(MessageConstants.USER_ID, expectedUserId);
        msg.set(MessageConstants.AMOUNT, Integer.toString(betAmount));

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(gameState);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(player).deductFromWallet(eq(expectedBetAmount));
    }

    @Test
    public void handleGameStateUpdateFoldCorrectly() {
        // ARRANGE
        String expectedUserId = "USER_ID";

        Message msg = new Message(MessageId.GameStateUpdateFold);
        msg.set(MessageConstants.USER_ID, expectedUserId);

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(Mockito.mock(GameState.class));

        // ACT & ASSERT
        assertDoesNotThrow(() -> gameSession.handleMessage(msg));
    }

    @Test
    public void handleGameStateUpdateCheckCorrectly() {
        // ARRANGE
        String expectedUserId = "USER_ID";

        Message msg = new Message(MessageId.GameStateUpdateCheck);
        msg.set(MessageConstants.USER_ID, expectedUserId);

        GameSession gameSession = new GameSession(null, null);

        // ACT & ASSERT
        assertDoesNotThrow(() -> gameSession.handleMessage(msg));
    }

    @Test
    public void handleGameStateUpdateHandOutcomeCorrectly() throws Exception {
        // ARRANGE
        String expectedUserId = "USER_ID";
        int expectedPotAmount = 101;

        Hand hand = new Hand(new Card(Suit.HEART, Rank.ACE), new Card(Suit.DIAMOND, Rank.ACE));

        Player player = Mockito.mock(Player.class);

        GameState gameState = Mockito.mock(GameState.class);
        Mockito.when( gameState.getPot() ).thenReturn( expectedPotAmount );
        Mockito.when( gameState.getPlayer(eq(expectedUserId)) ).thenReturn( player );

        Message msg = new Message(MessageId.GameStateUpdateHandOutcome);
        msg.set(MessageConstants.USER_ID, expectedUserId);
        msg.set(MessageConstants.HAND, hand);

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(gameState);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(player, times(1)).addToWallet(eq(expectedPotAmount));
        verify(gameState, times(1)).reset();
    }

    @Test
    public void handleGameStateUpdateHandOutcomeThrowsIfIncorrectUserId() {
        // ARRANGE
        String incorrectUserId = "USER_ID";

        Hand hand = new Hand(new Card(Suit.HEART, Rank.ACE), new Card(Suit.DIAMOND, Rank.ACE));

        GameState gameState = Mockito.mock(GameState.class);
        Mockito.when( gameState.getPlayer(eq(incorrectUserId)) ).thenReturn( null );

        Message msg = new Message(MessageId.GameStateUpdateHandOutcome);
        msg.set(MessageConstants.USER_ID, incorrectUserId);
        msg.set(MessageConstants.HAND, hand);

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(gameState);

        // ACT & ASSERT
        assertThrows(IllegalStateException.class, () -> gameSession.handleMessage(msg));
    }

    @Test
    public void handleGameStateUpdateJoinCorrectly() throws Exception {
        // ARRANGE
        String expectedUserId = "USER_ID";
        int expectedWallet = 100;
        int expectedPosition = 0;

        GameState gameState = Mockito.mock(GameState.class);

        Message msg = new Message(MessageId.GameStateUpdateJoin);
        msg.set(MessageConstants.USER_ID, expectedUserId);
        msg.set(MessageConstants.WALLET, expectedWallet);
        msg.set(MessageConstants.POSITION, expectedPosition);

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(gameState);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(gameState, times(1)).addPlayer(eq(expectedPosition), any(Player.class));
    }

    @Test
    public void handleGameStateUpdateLeaveCorrectly() throws Exception {
        // ARRANGE
        String expectedUserId = "USER_ID";

        GameState gameState = Mockito.mock(GameState.class);

        Message msg = new Message(MessageId.GameStateUpdateLeave);
        msg.set(MessageConstants.USER_ID, expectedUserId);

        GameSession gameSession = new GameSession(null, null);
        gameSession.setGameState(gameState);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(gameState, times(1)).removePlayer(eq(expectedUserId));
    }

    @Test
    public void handleActionRequestCorrectly() throws Exception {
        // ARRANGE
        long expectedTimestamp = 123456789;

        Message msg = new Message(MessageId.ActionRequest);
        msg.set(MessageConstants.TIMESTAMP, expectedTimestamp);

        GameSession gameSession = new GameSession(null, null);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        assertEquals(expectedTimestamp, gameSession.getLatestActionTimestamp());
    }

    @Test
    public void handleTextMessageBroadCastCorrectly() {
        // ARRANGE
        String expectedUserId = "USER_ID";
        String expectedMsg = "MESSAGE";

        Message msg = new Message(MessageId.TextMessageBroadCast);
        msg.set(MessageConstants.USER_ID, expectedUserId);
        msg.set(MessageConstants.MESSAGE, expectedMsg);

        GameSession gameSession = new GameSession(null, null);

        // ACT & ASSERT
        assertDoesNotThrow(() -> gameSession.handleMessage(msg));
    }

    @Test
    public void handleGameStateRequestCorrectly() throws Exception {
        // ARRANGE
        long expectedTimestamp = 123456789;

        Message msg = new Message(MessageId.GameStateRequest);
        msg.set(MessageConstants.TIMESTAMP, expectedTimestamp);

        ClientConnection connection = Mockito.mock(ClientConnection.class);
        GameSession gameSession = new GameSession(connection, null);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        assertEquals(expectedTimestamp, gameSession.getLatestGameStateTimestamp());
        verify(connection, times(1)).send(any(Message.class));
    }

    @Test
    public void handleActionResponseBetCorrectly() throws Exception {
        // ARRANGE
        Message msg = new Message(MessageId.ActionResponseBet);
        msg.set(MessageConstants.TIMESTAMP, 123L);
        msg.set(MessageConstants.AMOUNT, 456);

        ClientConnection connection = Mockito.mock(ClientConnection.class);
        GameState gameState = Mockito.mock(GameState.class);
        Mockito.when(
            gameState.getPlayer(isNull())
        ).thenReturn(
            new Player("DOES_NOT_MATTER", 1000)
        );

        GameSession gameSession = new GameSession(connection, null);
        gameSession.setGameState(gameState);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(connection, times(1)).send(any(Message.class));
    }

    @Test
    public void handleActionResponseCheckCorrectly() throws Exception {
        // ARRANGE
        Message msg = new Message(MessageId.ActionResponseCheck);
        msg.set(MessageConstants.TIMESTAMP, 123L);

        ClientConnection connection = Mockito.mock(ClientConnection.class);
        GameSession gameSession = new GameSession(connection, null);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(connection, times(1)).send(any(Message.class));
    }

    @Test
    public void handleActionResponseFoldCorrectly() throws Exception {
        // ARRANGE
        Message msg = new Message(MessageId.ActionResponseFold);
        msg.set(MessageConstants.TIMESTAMP, 123L);

        ClientConnection connection = Mockito.mock(ClientConnection.class);
        GameState gameState = Mockito.mock(GameState.class);

        GameSession gameSession = new GameSession(connection, null);
        gameSession.setGameState(gameState);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(connection, times(1)).send(any(Message.class));
        verify(gameState, times(1)).foldPlayer(isNull());
    }

    @Test
    public void handleTextMessageCorrectly() throws Exception {
        // ARRANGE
        Message msg = new Message(MessageId.TextMessage);
        msg.set(MessageConstants.MESSAGE, "textabc123");

        ClientConnection connection = Mockito.mock(ClientConnection.class);
        GameSession gameSession = new GameSession(connection, null);

        // ACT
        gameSession.handleMessage(msg);

        // ASSERT
        verify(connection, times(1)).send(any(Message.class));
    }
}
