package yabs.training.server;

/**
 * Represents a operation that can be done "on" a PlayerThread.
 */
public enum PlayerThreadOperation {
    CONNECT,
    DISCONNECT;
}
