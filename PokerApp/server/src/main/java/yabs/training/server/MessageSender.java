package yabs.training.server;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import yabs.training.common.Log;
import yabs.training.common.communication.CommunicationProtocol;
import yabs.training.common.communication.Message;

public class MessageSender {
    public static void broadcast(Map<String, PlayerThread> players, Message message, String exludedUserId) throws IOException {
        for(Entry<String, PlayerThread> entry : players.entrySet()) {
            String userId = entry.getKey();
            PlayerThread playerThread = entry.getValue();

            if(!userId.equals(exludedUserId)) {
                CommunicationProtocol.sendMessage(playerThread.getOutputStream(), message);
            }
        }

        Log.debug("Broadcasted message of type: " + message.getId().name());
    }

    public static void unicast(Map<String, PlayerThread> players, Message message, String recipientUserId) throws IOException {
        PlayerThread playerThread = players.get(recipientUserId);

        if (playerThread == null) {
            String errMsg = "Tried to sent unicast message to non existing player.";
            errMsg += " Recipient user ID: " + recipientUserId;
            throw new IllegalStateException(errMsg);
        }

        CommunicationProtocol.sendMessage(playerThread.getOutputStream(), message);
        Log.debug("Unicasted message to player \"" + recipientUserId + "\" of type: " + message.getId().name());
    }
}
