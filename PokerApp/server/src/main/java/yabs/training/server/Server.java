package yabs.training.server;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.cli.CommandLine;

import yabs.training.common.ArgumentParser;
import yabs.training.common.Log;
import yabs.training.common.Tuple;

public class Server {
    private static final int DEFAULT_SERVER_PORT = 4999;
    private static final int DEFAULT_MAX_PLAYER_NUMBER = 8;
    private static final int DEFAULT_WALLET_AMOUNT = 500;
    private static final int DEFAULT_SMALL_BLIND_AMOUNT = 10;
    private static final int DEFAULT_BIG_BLIND_AMOUNT = 20;

    public static void main(String[] args) throws Exception {
        CommandLine cmdOptions = ServerArgumentParser.parse(args);

        int serverPort = ArgumentParser.getIntOption(cmdOptions, ArgumentParser.SERVER_PORT, DEFAULT_SERVER_PORT);
        int wallet = ArgumentParser.getIntOption(cmdOptions, ArgumentParser.WALLET, DEFAULT_WALLET_AMOUNT);
        int maxPlayers = ArgumentParser.getIntOption(cmdOptions, ArgumentParser.MAX_PLAYER_NUMBER, DEFAULT_MAX_PLAYER_NUMBER);
        int bigBlind = ArgumentParser.getIntOption(cmdOptions, ArgumentParser.BIG_BLIND, DEFAULT_BIG_BLIND_AMOUNT);
        int smallBlind = ArgumentParser.getIntOption(cmdOptions, ArgumentParser.SMALL_BLIND, DEFAULT_SMALL_BLIND_AMOUNT);

        List<Thread> threads = new ArrayList<>(2);

        BlockingQueue<Tuple<PlayerThreadOperation, PlayerThread>> playerQueue = new LinkedBlockingQueue<>();
        PlayerMediator playerMediator = new PlayerMediator(playerQueue);

        ConnectionListener connectionListener = new ConnectionListener(playerMediator, serverPort);
        threads.add(new Thread(connectionListener));

        GameSessionManager gameSessionManager = new GameSessionManager(
            playerMediator,
            wallet,
            maxPlayers,
            bigBlind,
            smallBlind
        );
        threads.add(new Thread(gameSessionManager));

        for (Thread thread : threads) {
            thread.start();
        }

        Log.info("Server threads started.");

        for (Thread thread : threads) {
            thread.join();
        }

        Log.info("Server shutting down.");
    }
}
