package yabs.training.server;

import yabs.training.common.Card;
import yabs.training.common.CommunityCards;
import yabs.training.common.GameState;
import yabs.training.common.Hand;
import yabs.training.common.HandCalculations;
import yabs.training.common.Log;
import yabs.training.common.Player;
import yabs.training.common.communication.CommunicationProtocol;
import yabs.training.common.communication.GameStateMessage;
import yabs.training.common.communication.Message;
import yabs.training.common.communication.MessageConstants;
import yabs.training.common.communication.MessageId;
import yabs.training.common.Tuple;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class GameSession implements Runnable{

    private final String negativeBetMessage = "Error: Bet amount cannot be negative.";
    private final String insufficientBetMessage = "Error: You must match the current highest bet or go all-in.";
    private final String invalidCheckMessage = "Error: To be able to check, your must have bet equal to the current highest bet.";

    private BlockingQueue<Tuple<String, Message>> gameSessionQueue;
    private BlockingQueue<PlayerThread> spectators; // Queue containing users that are waiting to join the game
    private Set<String> spectatorIds; // A set containing userIds of spectators for constant time querying
    private Map<String, PlayerThread> players; // Key: User ID


    private Deck deck;
    private GameState gameState;

    private final int newPlayerWallet;
    private Long lastSentRequestTimeStamp;

    public GameSession(int newPlayerWallet, int maxPlayerNumber, int smallBlindAmount, int bigBlindAmount) {
        this.players = new HashMap<>();
        this.gameSessionQueue = new LinkedBlockingDeque<Tuple<String, Message>>();
        this.spectators = new LinkedBlockingDeque<PlayerThread>();
        this.spectatorIds = new HashSet<>();

        this.deck = new Deck();
        this.gameState = new GameState(maxPlayerNumber, smallBlindAmount, bigBlindAmount);
        this.newPlayerWallet = newPlayerWallet;
    }


    @Override
    public void run() {
        try {
            while (true) {
                Tuple<String, Message> msgTuple = this.gameSessionQueue.take();

                String senderId = msgTuple.getFirst();
                Message msg = msgTuple.getSecond();

                Log.debug("GameSession received msg: " + msg.getId().name());

                if(!players.containsKey(senderId) && msg.getId() != MessageId.Connected) {
                    throw new RuntimeException("Unrecognized sender ID: " + senderId);
                }

                // Do something with the message
                switch (msg.getId()) {
                    case TextMessage:
                        this.handleTextMessage(msg, senderId);
                        break;
                    case GameStateRequest:
                        this.handleGameStateRequest(msg, senderId);
                        break;
                    case ActionResponseBet:
                        this.handleActionResponseBet(msg, senderId);
                        break;
                    case ActionResponseCheck:
                        this.handleActionResponseCheck(msg, senderId);
                        break;
                    case ActionResponseFold:
                        this.handleActionResponseFold(msg, senderId);
                        break;
                    case Connected:
                        this.handleConnected(msg, senderId);
                        break;
                    case Disconnect:
                        this.handleDisconnect(msg, senderId);
                        break;
                    default:
                        // If nothing matches, ignore the message
                        break;
                }
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void handleGameStateRequest(Message msg, String senderId) throws IOException {
        msg.getId().assertEquals(MessageId.GameStateRequest);

        long receivedTimestamp = msg.getLong(MessageConstants.TIMESTAMP);
        GameStateMessage gameStateResponse = CommunicationProtocol.gameStateResponse(
            this.gameState,
            receivedTimestamp
        );

        PlayerThread playerThread = this.players.get(senderId);
        CommunicationProtocol.sendMessage(playerThread.getOutputStream(), gameStateResponse);

        Log.debug("Responded to a GameStateRequest from player \"" + senderId + "\"");
    }

    private void handleActionResponseBet(Message msg, String senderId) throws IOException {
        msg.getId().assertEquals(MessageId.ActionResponseBet);

        long receivedTimestamp = msg.getLong(MessageConstants.TIMESTAMP);
        long currentTimestamp = System.currentTimeMillis();

        // A player has made their bet, update the GameState

        if(!this.lastSentRequestTimeStamp.equals(receivedTimestamp)) {
            // If the timestamp doesn't match, ignore the message
            String errMsg = "Invalid timestamp (ActionResponseBet).";
            errMsg += " Expected: " + this.lastSentRequestTimeStamp;
            errMsg += ", got: " + receivedTimestamp;

            Log.error(errMsg);
            return;
        }

        PlayerThread playerThread = this.players.get(senderId);
        Player player = this.gameState.getPlayer(senderId);
        int betAmount = msg.getInt(MessageConstants.AMOUNT);

        if (betAmount < 0) {
            handleInvalidAction(playerThread, negativeBetMessage, receivedTimestamp, currentTimestamp);
        } else {
            int highestLiveBet = this.gameState.getHighestLiveBet();
            int playerCurrentTotalBet = this.gameState.getPlayerBet(senderId);

            // If a player is trying to bet more than they have,
            // set the actual bet amount equal to their wallet
            if(betAmount > player.getWallet()) {
                betAmount = player.getWallet();
            }

            int playerNewTotalBet = playerCurrentTotalBet + betAmount;

            // If player is not going all-in, they have to match the highest bet
            // If they can't, this is not a valid bet.
            if(betAmount < player.getWallet() && playerNewTotalBet < highestLiveBet) {
                handleInvalidAction(playerThread, insufficientBetMessage, receivedTimestamp, currentTimestamp);
            } else {
                player.deductFromWallet(betAmount);
                this.gameState.increasePot(betAmount);
                this.gameState.raisePlayerBet(senderId, betAmount);

                // Broadcast GameStateUpdate to all players
                Message betUpdate = CommunicationProtocol.gameStateUpdateBet(senderId, betAmount);
                // Broadcast message
                MessageSender.broadcast(players, betUpdate, senderId);
                Log.debug("Handled bet from player \"" + senderId + "\". Bet amount: " + betAmount);

                this.advanceGame();
            }
        }
    }

    private void handleActionResponseCheck(Message msg, String senderId) throws IOException {
        msg.getId().assertEquals(MessageId.ActionResponseCheck);

        // A player has checked, update the GameState

        long receivedTimestamp = msg.getLong(MessageConstants.TIMESTAMP);
        long currentTimestamp = System.currentTimeMillis();

        if(!this.lastSentRequestTimeStamp.equals(receivedTimestamp)) {
            // If the timestamp doesn't match, ignore the message
            String errMsg = "Invalid timestamp (ActionResponseCheck).";
            errMsg += " Expected: " + this.lastSentRequestTimeStamp;
            errMsg += ", got: " + receivedTimestamp;

            Log.debug(errMsg);
            return;
        }

        // For "Check" to be a valid action,
        // the player's current bet must be equal to the highest live bet.
        int highestLiveBet = this.gameState.getHighestLiveBet();
        int playerTotalBet = this.gameState.getPlayerBet(senderId);

        PlayerThread playerThread = this.players.get(senderId);

        if(playerTotalBet < highestLiveBet) {
            handleInvalidAction(playerThread, invalidCheckMessage, receivedTimestamp, currentTimestamp);
        } else {
            Message checkUpdate = CommunicationProtocol.gameStateUpdateCheck(senderId);
            MessageSender.broadcast(players, checkUpdate, senderId);

            Log.debug("Handled check from player \"" + senderId + "\"");
            this.advanceGame();
        }
    }


    private void handleActionResponseFold(Message msg, String senderId) throws IOException {
        msg.getId().assertEquals(MessageId.ActionResponseFold);

        // A player has folded, update the GameState

        long receivedTimestamp = msg.getLong(MessageConstants.TIMESTAMP);

        if(!this.lastSentRequestTimeStamp.equals(receivedTimestamp)) {
            // If the timestamp doesn't match, ignore the message
            String errMsg = "Invalid timestamp (ActionResponseFold).";
            errMsg += " Expected: " + this.lastSentRequestTimeStamp;
            errMsg += ", got: " + receivedTimestamp;

            Log.debug(errMsg);
            return;
        }

        this.gameState.foldPlayer(senderId);
        Message foldUpdate = CommunicationProtocol.gameStateUpdateFold(senderId);
        MessageSender.broadcast(players, foldUpdate, senderId);

        Log.debug("Handled fold from player \"" + senderId + "\"");
        this.advanceGame();
    }

    private void handleTextMessage(Message msg, String senderId) throws IOException {
        msg.getId().assertEquals(MessageId.TextMessage);

        String textMsg = msg.getString(MessageConstants.MESSAGE);
        Message broadcastMsg = CommunicationProtocol.textMessageBroadCast(senderId, textMsg);
        MessageSender.broadcast(players, broadcastMsg, senderId);
        Log.debug("Sent TextMessageBroadCast with content: " + textMsg);
    }

    private void handleConnected(Message msg, String senderId) throws IOException {
        msg.getId().assertEquals(MessageId.Connected);

        // A player has asked to join the game. If a game is currently on-going,
        // this request can be ignored and the player will be added to the table
        // at the end of the round.
        // If there are currently no active game (because there are not enough
        // players connected), this message will prompt the GameSession to try
        // and create a new game.
        if (this.players.size() >= 2) {
            return;
        }

        PlayerThread newPlayer = this.spectators.remove();
        this.spectatorIds.remove(newPlayer.getUserId());

        this.addPlayer(newPlayer);
        Log.debug("A new player was added to the game, userId: \"" + newPlayer.getUserId() + "\"");
        
        // If true: A second player was just added, a game can now be started.
        if (this.players.size() >= 2) {
            Log.debug("A new game is being started");
            this.startRound();
        }
    }

    private void handleDisconnect(Message msg, String senderId) throws IOException {
        msg.getId().assertEquals(MessageId.Disconnect);

        // A player has asked to leave the game
        // Remove them from the game
        this.removePlayer(senderId);
        // Let the other players know by sending out GameStateUpdateLeave messages
        Message leaveUpdate = CommunicationProtocol.gameStateUpdateLeave(senderId);
        // Broadcast message
        MessageSender.broadcast(players, leaveUpdate, senderId);

        // Edge case: If there is only one player left, that player wins
        if(players.size() == 1) {
            this.endRound();
        } else {
            this.advanceGame();
        }

        Log.debug("Handled disconnect from player \"" + senderId + "\"");
    }

    /**
     * @return The current number of players in the game.
     */
    public int getCurrentNumberOfPlayers() {
        return this.players.size();
    }

    /**
     * @return The number of players waiting in queue to join the game.
     */
    public int getCurrentNumberOfSpectators() {
        return this.spectators.size();
    }

    /**
     * @return The maximum number of players allowed in this game.
     */
    public int getMaxNumberOfPlayers() {
        return this.gameState.getMaxPlayerNumber();
    }

    /**
     * @return True if the number of vacant spots in the game is higher than the number of spectators in the queue.
     */
    public boolean hasVacantSpot() {
        int vacantSpots = this.getMaxNumberOfPlayers() - this.getCurrentNumberOfPlayers();
        return (vacantSpots > 0 && vacantSpots > this.getCurrentNumberOfSpectators());
    }

    /**
     * Checks whether a user with a given userId is connected to this GameSession.
     * @param userId The userId of the user we are looking for.
     * @return True if a user with the given userId is in the players map or in the spectators queue.
     */
    public boolean containsUser(String userId) {
        return (players.containsKey(userId) || spectatorIds.contains(userId));
    }

    /**
     * Adds a user to the queue as a spectator.
     * @param playerThread PlayerThread instance representing a player.
     * @throws IOException
     */
    public void addSpectator(PlayerThread playerThread) throws IOException {
        String userId = playerThread.getUserId();

        spectators.add(playerThread);
        spectatorIds.add(userId);

        // Add the connected player to the queue. This is the queue that `this`
        // GameSession is listening to, so it is used to notify it that a player
        // has connected.
        Message connectedMessage = CommunicationProtocol.connected(userId);
        this.gameSessionQueue.add(new Tuple<String, Message>(userId, connectedMessage));
    }

    // TODO: Implement properly, should be able to remove a spectator as well
    public void removePlayer(String playerId) {
        // Stop the connection in PlayerThread
        PlayerThread playerThread = this.players.get(playerId);

        // Remove the Player and related PlayerThread
        this.gameState.removePlayer(playerId);
        this.players.remove(playerId);
    }

    public void addPlayer(PlayerThread playerThread) throws IOException {
        // Get the player ID from the PlayerThread
        String userId = playerThread.getUserId();
        Player newPlayer = new Player(userId, this.newPlayerWallet);

        playerThread.setGameSessionQueue(this.gameSessionQueue);

        // Send the Connected message to client through PlayerThread
        Message connectedMessage = CommunicationProtocol.connected(userId);
        OutputStream outputStream = playerThread.getOutputStream();
        CommunicationProtocol.sendMessage(outputStream, connectedMessage);

        int position = this.gameState.getFirstVacantSpot();
        this.gameState.addPlayer(position, newPlayer);
        this.players.put(userId, playerThread);

        // Send GameStateUpdateJoin messages to the rest of the players
        Message joinMessage = CommunicationProtocol.gameStateUpdateJoin(userId, newPlayer.getWallet(), position);
        MessageSender.broadcast(players, joinMessage, userId);
    }

    private void startRound() throws IOException {
        while (!this.spectators.isEmpty()) {
            PlayerThread playerThread = this.spectators.remove();
            this.spectatorIds.remove(playerThread.getUserId());

            this.addPlayer(playerThread);
        }

        gameState.reset();
        this.deck.reset();

        /* Deal cards to players */
        for (Player player : gameState.getSittingPlayers()) {
            Card c1 = this.deck.next();
            Card c2 = this.deck.next();
            Hand hand = new Hand(c1, c2);
            player.setHand(hand);

            Message msg = CommunicationProtocol.gameStateUpdateNewHand(hand);
            MessageSender.unicast(players, msg, player.getUserID());
        }

        Player[] gameStatePlayers = this.gameState.getPlayers();

        /* Update small blind bet */
        int smallBlindIndex = this.gameState.getSmallBlindIndex();
        int smallBlindAmount = this.gameState.getSmallBlindAmount();
        Player smallBlindPlayer = gameStatePlayers[smallBlindIndex];
        String smallBlindUserId = smallBlindPlayer.getUserID();

        if (smallBlindPlayer.getWallet() < smallBlindAmount) {
            smallBlindAmount = smallBlindPlayer.getWallet();
        }

        smallBlindPlayer.deductFromWallet(smallBlindAmount);
        this.gameState.increasePot(smallBlindAmount);
        this.gameState.raisePlayerBet(smallBlindUserId, smallBlindAmount);

        Message smallBlindMsg = CommunicationProtocol.gameStateUpdateBet(smallBlindUserId, smallBlindAmount);
        MessageSender.broadcast(this.players, smallBlindMsg, null);

        /* Update big blind bet */
        int bigBlindIndex = this.gameState.getBigBlindIndex();
        int bigBlindAmount = this.gameState.getBigBlindAmount();
        Player bigBlindPlayer = gameStatePlayers[bigBlindIndex];
        String bigBlindUserId = bigBlindPlayer.getUserID();

        if (bigBlindPlayer.getWallet() < bigBlindAmount) {
            bigBlindAmount = bigBlindPlayer.getWallet();
        }

        bigBlindPlayer.deductFromWallet(bigBlindAmount);
        this.gameState.increasePot(bigBlindAmount);
        this.gameState.raisePlayerBet(bigBlindUserId, bigBlindAmount);

        Message bigBlindMsg = CommunicationProtocol.gameStateUpdateBet(bigBlindUserId, bigBlindAmount);
        MessageSender.broadcast(this.players, bigBlindMsg, null);

        /* Start the game by requesting a response from the button player */
        String buttonUserId = this.gameState.getCurrentPlayerID();
        this.lastSentRequestTimeStamp = System.currentTimeMillis();
        Message actionMsg = CommunicationProtocol.actionRequest(this.lastSentRequestTimeStamp);
        MessageSender.unicast(this.players, actionMsg, buttonUserId);
    }

    /**
     * Implements logic to end a poker round.
     * 
     * The winners are decided and the pot are divided evenly between them, there
     * are currently no logic to handle side pots.
     * 
     * The GameState is NOT reset/updated after a round. This is instead done
     * when a new round starts (see function `startRound()`).
     * 
     * @throws IOException if unable to broadcast result to clients.
     */
    private void endRound() throws IOException {
        CommunityCards communityCards = this.gameState.getCommunityCards();
        Player[] notFoldedPlayers = this.gameState.getPlayersNotFolded();

        Hand[] hands = new Hand[notFoldedPlayers.length];
        for (int i = 0; i < notFoldedPlayers.length; i++) {
            hands[i] = notFoldedPlayers[i].getHand();
        }

        Set<Integer> winnerIndices = HandCalculations.decideWinner(communityCards, hands);
        this.payoutWinners(notFoldedPlayers, winnerIndices);
    }

    private void advanceGame() throws IOException {
        // Fast-path if a single person hasn't folded, that player is the winner.
        Set<Integer> playersNotFolded = this.gameState.getPlayerIndicesNotFolded();
        if (playersNotFolded.size() == 1) {
            this.payoutWinners(this.gameState.getPlayers(), playersNotFolded);
            this.startRound();
            return;
        }

        int currentPlayerIndex = this.gameState.getCurrentPlayerIndex();
        int nextPlayerIndex = this.gameState.getNextPlayerIndex(currentPlayerIndex);
        int nextActivePlayerIndex = this.gameState.getNextActivePlayerIndex(currentPlayerIndex);

        // If the next player is the button holder (doesn't matter if the button
        // holder has folded or not), all players at the table have done their
        // actions for the turn (flop/turn/river).
        if (nextPlayerIndex == this.gameState.getButtonHolderIndex()) {
            Player lastPlayer = this.gameState.getPlayers()[nextActivePlayerIndex];
            String lastPlayerUserId = lastPlayer.getUserID();

            int lastPlayerBet = this.gameState.getPlayerBet(lastPlayerUserId);
            int highestBet = this.gameState.getHighestLiveBet();

            // If the next active player (the button holder if it hasn't folded
            // or the player after if the button holder has folded) has a bet
            // that is greater or equal to highest bet, it means that all player
            // have betted and "covers" the pot bet. Advance the game by adding
            // a new card to the table and start a new betting turn.
            //
            // Otherwise if the next active player has a bet that is lower than
            // the current highest bet, the game should continue around the table
            // and everyone will have a opportunity to either bet, raise or fold
            // again without a new card being drawn and shown.
            if (lastPlayerBet >= highestBet) {
                int nrCardsToDraw;

                // If five cards are shown on the table already, the round is
                // over and the winners can be decided. Otherwise just draw
                // and show a new card. If this is the flop, three cards will be
                // drawn.
                CommunityCards communityCards = this.gameState.getCommunityCards();
                if (communityCards.getCurrentNumberOfCards() >= 5) {
                    this.endRound();
                    this.startRound();
                    return;
                } else if (communityCards.getCurrentNumberOfCards() == 0) {
                    nrCardsToDraw = 3;
                } else {
                    nrCardsToDraw = 1;
                }

                while (nrCardsToDraw > 0) {
                    Card newCard = this.deck.next();
                    this.gameState.addCommunityCard(newCard);

                    Message newCardMsg = CommunicationProtocol.gameStateUpdateNewCard(newCard);
                    MessageSender.broadcast(this.players, newCardMsg, null);

                    nrCardsToDraw--;
                }
            }
        }

        this.gameState.moveToNextActivePlayer();
        String currentUserId = this.gameState.getCurrentPlayerID();
        this.lastSentRequestTimeStamp = System.currentTimeMillis();
        Message actionMsg = CommunicationProtocol.actionRequest(this.lastSentRequestTimeStamp);
        MessageSender.unicast(this.players, actionMsg, currentUserId);
    }

    /**
     * Pays out the winner(s) of a round and notifies all player about it.
     * The `winnerIndices` are the indices of the players in `players` that
     * have won the round.
     * 
     * @param players a list of players that have played a round.
     * @param winnerIndices the indices (of `players`) for winners of a round.
     * @throws IOException
     */
    private void payoutWinners(Player[] players, Set<Integer> winnerIndices) throws IOException {
        List<String> winnerUserIds = new ArrayList<>(winnerIndices.size());
        for (Integer winnerIndex : winnerIndices) {
            String winnerUserId = players[winnerIndex].getUserID();
            winnerUserIds.add(winnerUserId);
        }

        int amountOfWinners = winnerUserIds.size();
        // TODO: What to do about rounding errors?
        int potPerWinner = gameState.getPot() / amountOfWinners;

        for (String winnerUserId : winnerUserIds) {
            gameState.getPlayer(winnerUserId).addToWallet(potPerWinner);
        }

        // TODO: gameStateUpdateHandOutcome currently only allows to send a
        //       single winner, while in reality there can be multiple winners.
        //       For now just send the first player.
        String firstWinnerUserId = winnerUserIds.iterator().next();
        Hand firstWinnerHand = gameState.getPlayer(firstWinnerUserId).getHand();

        Message handOutcomeMessage = CommunicationProtocol.gameStateUpdateHandOutcome(
            firstWinnerUserId,
            firstWinnerHand
        );
        MessageSender.broadcast(this.players, handOutcomeMessage, null);
    }

    /**
     * Handles sending an error message and a new action request in case of an invalid action response.
     * @param playerThread Thread representing the player to communicate through.
     * @param message Content of the error message to be sent.
     * @param receivedTimestamp Timestamp from the invalid action message received from the player.
     * @param currentTimestamp The current timestamp to put in the new action request that will be sent.
     * @throws IOException If cannot get OutputStream from PlayerThread or message cannot be sent.
     */
    private void handleInvalidAction(PlayerThread playerThread, String message, long receivedTimestamp, long currentTimestamp)
    throws IOException {
        Message errorMessage = CommunicationProtocol.error(message, receivedTimestamp);

        Message newActionRequest = CommunicationProtocol.actionRequest(currentTimestamp);
        this.lastSentRequestTimeStamp = currentTimestamp;

        // Send error message to player
        CommunicationProtocol.sendMessage(playerThread.getOutputStream(), errorMessage);
        // Send new ActionRequest to player
        CommunicationProtocol.sendMessage(playerThread.getOutputStream(), newActionRequest);
        Log.error("Handled invalid action. Error message: " + message);
    }
}
