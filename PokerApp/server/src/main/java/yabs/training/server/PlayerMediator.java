package yabs.training.server;

import java.util.concurrent.BlockingQueue;

import yabs.training.common.Tuple;

/**
 * Used to send information related to PlayerThreads to a given queue.
 */
public class PlayerMediator {
    private final BlockingQueue<Tuple<PlayerThreadOperation, PlayerThread>> playerQueue;

    public PlayerMediator(BlockingQueue<Tuple<PlayerThreadOperation, PlayerThread>> playerQueue) {
        this.playerQueue = playerQueue;
    }

    /**
     * OBS! This is a blocking operation. The caller of this function will wait
     * until a "notification" is sent to this PlayerMediator.
     */
    public Tuple<PlayerThreadOperation, PlayerThread> take() throws InterruptedException {
        return this.playerQueue.take();
    }

    public void notifyConnect(PlayerThread playerThread) throws InterruptedException {
        this.put(PlayerThreadOperation.CONNECT, playerThread);
    }

    public void notifyDisconnect(PlayerThread playerThread) throws InterruptedException {
        this.put(PlayerThreadOperation.DISCONNECT, playerThread);
    }

    private void put(PlayerThreadOperation op, PlayerThread playerThread) throws InterruptedException {
        this.playerQueue.put(new Tuple<>(op, playerThread));
    }
}
