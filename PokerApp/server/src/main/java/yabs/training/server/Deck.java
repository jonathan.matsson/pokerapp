package yabs.training.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import yabs.training.common.Card;
import yabs.training.common.Rank;
import yabs.training.common.Suit;

/**
 * Represents a deck.
 */
public class Deck {
    /**
     * The amount of cards in a regular poker deck.
     */
    public static final int DECK_SIZE = 52;

    /**
     * The position/index of the card at the top of the imaginary deck. The cards
     * will not be moved in the `cards` list, instead the position will start
     * from 0 and be incremented for every pulled card.
     * 
     * When this position reaches `DECK_SIZE`, all cards have been drawn and the
     * deck is considered empty.
     */
    private int position;

    private final List<Card> cards;

    public Deck() {
        this.cards = new ArrayList<>(DECK_SIZE);

        // Create all cards by combining all possible suits with all possible ranks.
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                this.cards.add(new Card(suit, rank));
            }
        }

        this.reset();
    }

    /**
     * Resets the `position` of the deck to its initial value and shuffles the deck.
     */
    public void reset() {
        this.position = 0;
        Collections.shuffle(this.cards);
    }

    /**
     * Pulls the next card of the deck. If there are no more cards left in the deck,
     * returns null.
     * 
     * @return the card at the top of the deck or null if empty.
     */
    public Card next() {
        if (this.position >= this.cards.size()) {
            return null;
        }

        Card card = this.cards.get(this.position);
        this.position++;

        return card;
    }
}
