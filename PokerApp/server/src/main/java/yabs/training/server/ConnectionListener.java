package yabs.training.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import yabs.training.common.Log;

public class ConnectionListener implements Runnable {
    private static final int NUMBER_OF_THREADS = 8;
    private static final int DEFAUL_PORT = 4999;

    private final PlayerMediator mediator;
    private final int port;
    private final ExecutorService threadPool;

    private int userIdNumber;

    public ConnectionListener(PlayerMediator mediator) {
        this(mediator, DEFAUL_PORT);
    }

    public ConnectionListener(PlayerMediator mediator, int port) {
        this(mediator, port, Executors.newFixedThreadPool(NUMBER_OF_THREADS));
    }

    public ConnectionListener(PlayerMediator mediator, int port, ExecutorService threadPool) {
        this.mediator = mediator;
        this.port = port;
        this.threadPool = threadPool;

        this.userIdNumber = 0;
    }

    // TODO: How should the user IDs be created/fetched?
    private String produceUserId() {
        return Integer.toString(this.userIdNumber++);
    }

    @Override
    public void run() {
        try {

            while (true) {
                try (ServerSocket serverSocket = new ServerSocket(this.port)) {
                    Socket clientSocket = serverSocket.accept();

                    String userId = this.produceUserId();
                    PlayerThread playerThread = new PlayerThread(clientSocket, userId, this.mediator);

                    this.threadPool.execute(playerThread);
                    this.mediator.notifyConnect(playerThread);

                    Log.debug("A new client has been connected, userId: " + userId);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.threadPool.shutdownNow();
        }
    }
}
