package yabs.training.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import yabs.training.common.Log;
import yabs.training.common.Tuple;

/**
 * This class is in charge of managing GameSession objects. The GameSessionManager
 * will be the "ultimate" owner of all GameSession and PlayerThread instances.
 * 
 * If a player connects/disconnects or a game session starts/ends, it will be
 * communicated through this class.
 */
public class GameSessionManager implements Runnable {
    /**
     * The GameSessionManager thread will block in this mediator. The result
     * will contain a reference to a PlayerThread and the operation that should
     * be "performed" by this manager.
     * 
     * See the class "PlayerThreadOperation" for possible actions.
     */
    private final PlayerMediator playerMediator;

    /**
     * This map will contain all active connections to players.
     * The key is the user ID representing the player thread.
     */
    private final Map<String, PlayerThread> playerThreads;

    /**
     * Contains all active game sessions.
     */
    private final List<GameSession> gameSessions;

    /**
     * Settings to use when this GameSessionManager creates a new GameSession.
     */
    private final int newPlayerWallet;
    private final int maxPlayerNumber;
    private final int smallBlindAmount;
    private final int bigBlindAmount;

    public GameSessionManager(PlayerMediator playerMediator, int newPlayerWallet, int maxPlayerNumber,
                              int smallBlindAmount, int bigBlindAmount)
    {
        this.playerMediator = playerMediator;
        this.playerThreads = new HashMap<>();
        this.gameSessions = new ArrayList<>();

        this.newPlayerWallet = newPlayerWallet;
        this.maxPlayerNumber = maxPlayerNumber;
        this.smallBlindAmount = smallBlindAmount;
        this.bigBlindAmount = bigBlindAmount;
    }

    @Override
    public void run() {
        try {

            while (true) {
                // This `take()` call is blocking.
                Tuple<PlayerThreadOperation, PlayerThread> tuple = this.playerMediator.take();

                PlayerThreadOperation operation = tuple.getFirst();
                PlayerThread playerThread = tuple.getSecond();

                switch (operation) {
                    case CONNECT:
                        this.addPlayer(playerThread);
                        break;
                    case DISCONNECT:
                        this.removePlayer(playerThread);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected operation: " + operation);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    /**
     * Adds a new player represented by `playerThread` to a GameSession.
     * 
     * The player will be added to the first GameSession which isn't full.
     * If there are no active GameSession's running, this function will create
     * a new one and add the player to that session.
     * 
     * @param playerThread the player to add to a GameSession.
     * @throws IOException if unable to send connect/join messages to other players.
     */
    private void addPlayer(PlayerThread playerThread) throws IOException {
        this.playerThreads.put(playerThread.getUserId(), playerThread);

        Log.debug("Adding player \"" + playerThread.getUserId() + "\" to GameSession.");

        GameSession chosenGameSession = null;

        for (GameSession gameSession : this.gameSessions) {
            if (gameSession.hasVacantSpot()) {
                chosenGameSession = gameSession;
                break;
            }
        }

        // If true: No available GameSession found. This can either be because no
        // GameSession exists or it can also be because all GameSession's are full.
        // In this case, need to create a new one that this player can join.
        if (chosenGameSession == null) {
            chosenGameSession = new GameSession(
                this.newPlayerWallet,
                this.maxPlayerNumber,
                this.smallBlindAmount,
                this.bigBlindAmount
            );
            this.gameSessions.add(chosenGameSession);

            // TODO: Need to keep track of these threads somewhere.
            new Thread(chosenGameSession).start();
        }

        chosenGameSession.addSpectator(playerThread);
    }

    /**
     * Removes the player represented by `playerThread` from all active GameSessions.
     * 
     * @param playerThread the player to remove.
     */
    private void removePlayer(PlayerThread playerThread) {
        String userId = playerThread.getUserId();
        this.playerThreads.remove(userId);

        Log.debug("Removing player \"" + playerThread.getUserId() + "\" from GameSession.");

        for (GameSession gameSession : this.gameSessions) {
            if (gameSession.containsUser(userId)) {
                gameSession.removePlayer(userId);
            }
        }
    }
}
