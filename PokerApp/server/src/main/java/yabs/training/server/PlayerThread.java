package yabs.training.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

import yabs.training.common.Tuple;
import yabs.training.common.communication.CommunicationProtocol;
import yabs.training.common.communication.Message;

public class PlayerThread implements Runnable {
    private final PlayerMediator mediator;
    private final Socket clientSocket;
    private final String userId;

    private BlockingQueue<Tuple<String, Message>> gameSessionQueue;

    public PlayerThread(Socket clientSocket, String userId, PlayerMediator mediator) throws IOException {
        this.clientSocket = clientSocket;
        this.userId = userId;
        this.mediator = mediator;
    }

    public String getUserId() {
        return this.userId;
    }

    public InputStream getInputStream() throws IOException {
        return this.clientSocket.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        return this.clientSocket.getOutputStream();
    }

    public void setGameSessionQueue(BlockingQueue<Tuple<String, Message>> gameSessionQueue) {
        this.gameSessionQueue = gameSessionQueue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Message message = CommunicationProtocol.readMessage(this.getInputStream());
                this.gameSessionQueue.put(new Tuple<>(this.userId, message));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                this.mediator.notifyDisconnect(this);
                this.clientSocket.close();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
