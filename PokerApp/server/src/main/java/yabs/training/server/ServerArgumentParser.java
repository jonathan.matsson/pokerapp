package yabs.training.server;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import yabs.training.common.ArgumentParser;

public class ServerArgumentParser {
    public static CommandLine parse(String[] args) {
        Options options = new Options();
        Option option;

        option = new Option("a", ArgumentParser.SERVER_ADDRESS, true, "The hostname/IP of the server.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("p", ArgumentParser.SERVER_PORT, true, "The port of the server.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("w", ArgumentParser.WALLET, true, "The amount of chips given to new players joining a table.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("m", ArgumentParser.MAX_PLAYER_NUMBER, true, "The max amount of players that can sit at a table.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("b", ArgumentParser.BIG_BLIND, true, "The big blind amount.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("s", ArgumentParser.SMALL_BLIND, true, "The small blind amount.");
        option.setRequired(false);
        options.addOption(option);

        return ArgumentParser.parse(options, args, "PokerApp Server");
    }
}
