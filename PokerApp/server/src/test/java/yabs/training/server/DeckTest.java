package yabs.training.server;

import org.junit.jupiter.api.Test;

import yabs.training.common.Card;
import yabs.training.common.Rank;
import yabs.training.common.Suit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

public class DeckTest {
    @Test
    public void allCardsAreCreatedCorrectly() {
        // ARRANGE
        List<Card> expectedCards = new ArrayList<>();

        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                expectedCards.add(new Card(suit, rank));
            }
        }

        Deck deck = new Deck();

        // ACT & ASSERT

        // Iterate through all cards of the deck. Assert that the card can be
        // found in the `expectedCards`. If it is found, remove it from the
        // `expectedCards`. At the end of this test, the `expectedCards` should
        // be empty since all cards should have been "found"/"seen".
        Card actualCard;
        while ((actualCard = deck.next()) != null) {
            Suit actualSuit = actualCard.getSuit();
            Rank actualRank = actualCard.getRank();

            boolean cardFound = false;

            for (Card expectedCard : expectedCards) {
                Suit expectedSuit = expectedCard.getSuit();
                Rank expectedRank = expectedCard.getRank();

                if (expectedSuit == actualSuit && expectedRank == actualRank) {
                    cardFound = true;
                    expectedCards.remove(expectedCard);
                    break;
                }
            }

            assertTrue(cardFound);
        }

        assertEquals(0, expectedCards.size());
    }

    @Test
    public void deckContainsExactly52Cards() {
        // ARRANGE
        int expectedCount = 52;
        int actualCount;

        Deck deck = new Deck();

        // ACT
        actualCount = 0;
        while (deck.next() != null) {
            actualCount++;
        }

        // ASSERT
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void resetStartsOverWithAll52Cards() {
        // ARRANGE
        int expectedCount = 52;
        int actualCount;

        Deck deck = new Deck();

        // Pull all cards of the deck until all of them has been removed.
        while (deck.next() != null) {}

        // ACT
        deck.reset();

        actualCount = 0;
        while (deck.next() != null) {
            actualCount++;
        }

        // ASSERT
        assertEquals(expectedCount, actualCount);
    }
}
