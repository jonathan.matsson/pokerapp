package yabs.training.common;

public class CommunityCards {

    private Card[] cards;
    private int currentNumberOfCards;

    public CommunityCards() {
        this.cards = new Card[5];
        this.currentNumberOfCards = 0;
    }

    public void addCard(Card card) {
        this.cards[currentNumberOfCards] = card;
        this.currentNumberOfCards = this.currentNumberOfCards + 1;
    }

    public Card[] getCards() {
        return this.cards;
    }

    public int getCurrentNumberOfCards() {
        return this.currentNumberOfCards;
    }

    @Override
    public String toString() {
        final int nrOfCards = this.getCurrentNumberOfCards(); 

        if (nrOfCards > 0) {
            Card[] cards = this.getCards();

            String[] cardStrings = new String[nrOfCards];
            for (int i = 0; i < nrOfCards; i++) {
                cardStrings[i] = cards[i].toString();
            }

            return String.join(", ", cardStrings);
        } else {
            return "No cards on the table.";
        }
    }
}
