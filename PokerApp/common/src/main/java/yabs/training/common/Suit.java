package yabs.training.common;

/**
 * Represents the suit of a card.
 */
public enum Suit {
    // TODO: Possible to use unicode character? Windows cmd doesn't support
    //       it, so might not be possible, but would look cleaner.
    SPADE("Spade"),
    HEART("Heart"),
    DIAMOND("Diamond"),
    CLUB("Club");

    /**
     * Textual representation of the suit.
     */
    private final String text;

    private Suit(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
