package yabs.training.common;

/**
 * Class representing a card.
 */
public class Card  {
    private final Suit suit;
    private final Rank rank;

    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public Suit getSuit() {
        return this.suit;
    }

    public Rank getRank() {
        return this.rank;
    }

    @Override
    public String toString() {
        return "Card(" + this.getSuit() + ", " + this.getRank() + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else if (this == other) {
            return true;
        } else if (!(other instanceof Card)) {
            return false;
        }

        Card o = (Card)other;
        return this.suit == o.suit && this.rank == o.rank;
    }
}