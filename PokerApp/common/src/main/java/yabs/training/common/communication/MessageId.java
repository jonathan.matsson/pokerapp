package yabs.training.common.communication;

/**
 * The message ID that identifies the kind/type of a message. 
 */
public enum MessageId {
    GameStateRequest("1.1"),
    GameStateResponse("1.2"),
    GameStateUpdateBet("1.3.1"),
    GameStateUpdateFold("1.3.2"),
    GameStateUpdateCheck("1.3.3"),
    GameStateUpdateHandOutcome("1.3.4"),
    GameStateUpdateJoin("1.3.5"),
    GameStateUpdateLeave("1.3.6"),
    GameStateUpdateNewCard("1.3.7"),
    GameStateUpdateNewHand("1.3.8"),
    Disconnect("2"),
    ActionRequest("3.1"),
    ActionResponseBet("3.2.1"),
    ActionResponseFold("3.2.2"),
    ActionResponseCheck("3.2.3"),
    TextMessage("4.1"),
    TextMessageBroadCast("4.2"),
    Connected("5"),
    Error("6");

    private final String id;

    private MessageId(String id) {
        this.id = id;
    }

    /**
     * Given a string `id`, returns the enum variant representing the given `id.`
     *
     * @param id the ID to convert to a enum.
     * @return enum representation of `id`.
     */
    public static MessageId parseId(String id) {
        for (MessageId e : MessageId.values()) {
            if (id.equalsIgnoreCase(e.id)) {
                return e;
            }
        }

        throw new IllegalArgumentException("Invalid ID found in message: " + id);
    }

    /**
     * Asserts that the given `otherId` is equals to `this` ID.
     * 
     * @param otherId the ID to compare to `this` ID.
     * @throws IllegalStateException if the IDs are different.
     */
    public void assertEquals(MessageId otherId) throws IllegalStateException {
        if (this != otherId) {
            String errMsg = "Expected \"" + otherId.name() + "\" message, got: " + this.name();
            throw new IllegalStateException(errMsg);
        }
    }

    @Override
    public String toString() {
        return this.id;
    }
}