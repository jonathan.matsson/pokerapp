package yabs.training.common;

import java.io.Serializable;

public class Player implements Serializable {

    private String userID;
    private int wallet;
    private Hand hand;

    public Player(String userID, int wallet) {
        this.userID = userID;
        this.wallet = wallet;
        this.hand = null;
    }

    public String getUserID() {
        return this.userID;
    }

    public int getWallet() {
        return this.wallet;
    }

    public Hand getHand() {
        return this.hand;
    }

    public void setHand(Hand newHand) {
        this.hand = newHand;
    }

    public void addToWallet(int amount) {
        if (amount > 0) {
            try {
                this.wallet = Math.addExact(this.wallet, amount);
            } catch (ArithmeticException e) {
                this.wallet = Integer.MAX_VALUE;
            }
        }
    }

    public void deductFromWallet(int amount) {
        if (this.wallet - amount < 0) {
            this.wallet = 0;
        } else {
            this.wallet -= amount;
        }
    }
}
