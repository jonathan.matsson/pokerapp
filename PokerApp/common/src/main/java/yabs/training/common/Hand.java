package yabs.training.common;

public class Hand {

    private final Card firstCard;
    private final Card secondCard;

    public Hand(Card first, Card second) {
        this.firstCard = first;
        this.secondCard = second;
    }

    public Card getFirstCard() {
        return this.firstCard;
    }

    public Card getSecondCard() {
         return this.secondCard;
    }

    @Override
    public String toString() {
        return "Hand(" + this.getFirstCard() + ", " + this.getSecondCard() + ")";
    }
}
