package yabs.training.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class ArgumentParser {
    public static final String SERVER_ADDRESS    = "address";
    public static final String SERVER_PORT       = "port";
    public static final String WALLET            = "wallet";
    public static final String MAX_PLAYER_NUMBER = "players";
    public static final String BIG_BLIND         = "big";
    public static final String SMALL_BLIND       = "small";

    public static CommandLine parse(Options options, String[] args, String appName) {
        CommandLine cmd = null;
        try {
            cmd = new DefaultParser().parse(options, args);
        } catch (ParseException e) {
            System.err.print(getUsagePrint(options, appName));
            System.exit(1);
        }
        return cmd;
    }

    public static int getIntOption(CommandLine cmdOptions, String optionName, int defaultValue)
    throws IllegalArgumentException {
        String value = cmdOptions.getOptionValue(optionName, String.valueOf(defaultValue));
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            String msg = "Unable to parse option with name \"" + optionName + "\"";
            msg += " as an integer. The value specified was: " + value;
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Defaults to `localhost`.
     */
    public static InetAddress getInetAddressOption(CommandLine cmdOptions, String optionName)
    throws UnknownHostException {
        // A `InetAddress::getByName` with a null `value` uses a loopback address.
        String value = cmdOptions.getOptionValue(optionName);
        return InetAddress.getByName(value);
    }

    private static String getUsagePrint(Options options, String appName) {
        String usage;

        try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {

            HelpFormatter hf = new HelpFormatter();
            hf.printHelp(pw, hf.getWidth(), appName, null, options,
                         hf.getLeftPadding(), hf.getDescPadding(), null, true);
            pw.flush();
            usage = sw.toString();

        } catch (IOException ex) {
            String msg = "Unable to close StringWriter during usage printing.";
            System.err.println(msg + "\n" + ex);
            usage = "";
        }

        return usage;
    }
}