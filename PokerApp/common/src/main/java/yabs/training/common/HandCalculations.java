package yabs.training.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class HandCalculations {
    public enum HandRank {
        NOTHING,
        PAIR,
        TWO_PAIR,
        THREE_OF_A_KIND,
        STRAIGHT,
        FLUSH,
        FULL_HOUSE,
        FOUR_OF_A_KIND,
        STRAIGHT_FLUSH;
    }

    public enum WinningHand {
        FIRST,  // The first hand (first argument) has the highest card(s).
        SECOND, // The second hand (second argument) has the highest card(s).
        EQUAL;  // Both hands have the same highest card(s).
    }

    private static class Tuple<T1, T2> {
        public final T1 first;
        public final T2 second;

        public Tuple(T1 first, T2 second) {
            this.first = first;
            this.second = second;
        }
    }

    /**
     * Calculates the hand rank of the given cards.
     * The given `cards` should be sorted by their ranks. The overloaded function
     * `handRank(CommunityCards, Hand)` will be a better alternative to call in
     * most cases since it handles the creation and sorting of the `cards` internally.
     * 
     * @return the HandRank for the the given cards.
     */
    public static final HandRank handRank(Card[] cards) {
        List<Tuple<Rank, Integer>> duplicateCounts = countDuplicates(cards);

        if (isFlush(cards) && isStraight(cards)) {
            return HandRank.STRAIGHT_FLUSH;
        } else if (isFourOfAKind(duplicateCounts)) {
            return HandRank.FOUR_OF_A_KIND;
        } else if (isFullHouse(duplicateCounts)) {
            return HandRank.FULL_HOUSE;
        } else if (isFlush(cards)) {
            return HandRank.FLUSH;
        } else if (isStraight(cards)) {
            return HandRank.STRAIGHT;
        } else if (isThreeOfAKind(duplicateCounts)) {
            return HandRank.THREE_OF_A_KIND;
        } else if (isTwoPair(duplicateCounts)) {
            return HandRank.TWO_PAIR;
        } else if (isPair(duplicateCounts)) {
            return HandRank.PAIR;
        } else {
            return HandRank.NOTHING;
        }
    }

    public static final HandRank handRank(CommunityCards communityCards, Hand hand) {
        return handRank(buildSortedCards(communityCards, hand));
    }

    /**
     * Decides the winner of the given `hands`. The index of the winner in the
     * `hands` array is returned.
     * 
     * @param communityCards
     * @param hands
     * @return
     */
    public static Set<Integer> decideWinner(CommunityCards communityCards, Hand... hands) {
        List<Tuple<HandRank, Integer>> handRanks = new ArrayList<>();

        // Go through the hands and calculate the "HandRank"s. The handRank will
        // be put into the `handRanks` list grouped together with its index.
        for (int i = 0; i < hands.length; i++) {
            Card[] cards = buildSortedCards(communityCards, hands[i]);
            HandRank handRank = handRank(cards);

            handRanks.add(new Tuple<>(handRank, i));
        }

        // Sort the hands. If two hands have the same HandRank, sort them by
        // the highest card(s). After the list has been sorted, the winner will
        // be at the first position of the list.
        handRanks.sort((a, b) -> {
            HandRank aHandRank = a.first;
            HandRank bHandRank = b.first;

            if (aHandRank != bHandRank) {
                return (aHandRank.ordinal() > bHandRank.ordinal()) ? -1 : 1;
            } else {
                // Both hands have the same HandRank, decide winner by highest card(s).
                Hand handA = hands[a.second];
                Hand handB = hands[b.second];

                // `aHandRank` == `bHandRank`, just use one arbitrarily.
                switch (decideHighest(communityCards, handA, handB, aHandRank)) {
                    case FIRST:  return -1;
                    case SECOND: return 1;
                    case EQUAL:  return 0;
                    default:     return 0;
                }
            }
        });

        // There might be multiple people with the same exact hand strength.
        // In those cases, there will be multiple winners. Create a set that
        // will contain all winners and be returned from this function.
        Set<Integer> winningIndices = new HashSet<>();

        // Since the hands in `handRanks` are sorted according to their strength,
        // the first hand is always a winner. One can iterate through the hands
        // one by one from the start and ones a weaker hand is found, it is known
        // that any hands coming after it will be weaker or equally weak.
        HandRank winnerHandRank = handRanks.get(0).first;
        int winnerIndex = handRanks.get(0).second;
        winningIndices.add(winnerIndex);

        for (int i = 1; i < handRanks.size(); i++) {
            HandRank currentHandRank = handRanks.get(i).first;
            int currentIndex = handRanks.get(i).second;

            // If the hand ranks are the same, check if the highest cards are
            // equal as well. If they are equal, add the current index to the
            // `winningIndices` and "continue" looking through the hands to see
            // if more hands are winners.
            if (currentHandRank == winnerHandRank) {
                WinningHand winningHand = decideHighest(
                    communityCards,
                    hands[currentIndex],
                    hands[winnerIndex],
                    winnerHandRank
                );

                if (winningHand == WinningHand.EQUAL) {
                    winningIndices.add(currentIndex);
                    continue;
                }
            }

            // Found hand that isn't equal to the winning hand, break out of
            // the loop, there are no other winners.
            break;
        }

        return winningIndices;
    }

    /**
     * Given that two hands have the same HandRank, this function decides the
     * winner of the two hands by finding the hand with the highest cards.
     * 
     * @param communityCards
     * @param handA
     * @param handB
     * @param handRank
     * @return the winner of the two hands or if they are equaly strong.
     */
    private static WinningHand decideHighest(CommunityCards communityCards, Hand handA,
                                             Hand handB, HandRank handRank)
    {
        Card[] cardsA = buildSortedCards(communityCards, handA);
        Card[] cardsB = buildSortedCards(communityCards, handB);

        WinningHand winningHand;

        switch (handRank) {
            case NOTHING:
            case PAIR:
            case TWO_PAIR:
            case THREE_OF_A_KIND:
            case FOUR_OF_A_KIND:
            case FULL_HOUSE:
                List<Tuple<Rank, Integer>> duplicateCountsA = countDuplicates(cardsA);
                List<Tuple<Rank, Integer>> duplicateCountsB = countDuplicates(cardsB);
                winningHand = decideHighest(duplicateCountsA, duplicateCountsB);
                break;

            case STRAIGHT:
            case STRAIGHT_FLUSH:
                Rank rankA = calculateStraight(cardsA);
                Rank rankB = calculateStraight(cardsB);
                winningHand = decideHighest(rankA, rankB);
                break;

            case FLUSH:
                Rank[] ranksA = calculateFlush(cardsA);
                Rank[] ranksB = calculateFlush(cardsB);
                winningHand = decideHighest(ranksA, ranksB);
                break;

            default:
                String msg = "Unreachable, all enum cases already covered.";
                throw new IllegalStateException(msg);
        }

        return winningHand;
    }

    /**
     * Decides the winning hand between hands that have the same HandRank.
     * This is done by looking at the rank of the hands and deciding which hand
     * has the highest ranks.
     */
    private static WinningHand decideHighest(Rank[] ranksA, Rank[] ranksB) {
        if (ranksA.length != ranksB.length) {
            String msg = "Lengths of ranks different. Length ranksA: ";
            msg +=  ranksA.length + ", length  ranksB: " + ranksB.length;
            throw new IllegalArgumentException(msg);
        }

        WinningHand winningHand = WinningHand.EQUAL;

        for (int i = 0; i < ranksA.length; i++) {
            Rank rankA = ranksA[i];
            Rank rankB = ranksB[i];

            if (rankA.ordinal() > rankB.ordinal()) {
                winningHand = WinningHand.FIRST;
                break;
            } else if (rankA.ordinal() < rankB.ordinal()) {
                winningHand = WinningHand.SECOND;
                break;
            }
        }

        return winningHand;
    }

    /**
     * Decides the winning hand between hands that have the same HandRank.
     * This is done by looking at the rank of the hands and deciding which hand
     * has the highest ranks.
     */
    private static WinningHand decideHighest(Rank rankA, Rank rankB) {
        return decideHighest(new Rank[] { rankA }, new Rank[] { rankB });
    }

    /**
     * Decides the winning hand between hands that have the same HandRank.
     * This is done by looking at the rank of the hands and deciding which hand
     * has the highest ranks.
     */
    private static WinningHand decideHighest(List<Tuple<Rank, Integer>> duplicateCountsA,
                                             List<Tuple<Rank, Integer>> duplicateCountsB)
    {
        // Should only look at the best/first 5 cards. If a winner can't be
        // decided after 5 cards have been "seen", rank the hands as "equal".
        int amountOfCountedCards = 0;

        WinningHand winningHand = null;

        for (int i = 0; i < duplicateCountsA.size(); i++) {
            Rank rankA = duplicateCountsA.get(i).first;
            Rank rankB = duplicateCountsB.get(i).first;

            if (rankA.ordinal() > rankB.ordinal()) {
                winningHand = WinningHand.FIRST;
                break;
            } else if (rankA.ordinal() < rankB.ordinal()) {
                winningHand = WinningHand.SECOND;
                break;
            }

            // Count of "A" and "B" is equal, so pick one of them arbitrarily.
            int count = duplicateCountsA.get(i).second;
            amountOfCountedCards += count;

            if (amountOfCountedCards >= 5) {
                winningHand = WinningHand.EQUAL;
                break;
            }
        }

        return winningHand;
    }

    protected static boolean isPair(List<Tuple<Rank, Integer>> duplicateCounts) {
        return isCountMatch(duplicateCounts, 2);
    }

    protected static boolean isTwoPair(List<Tuple<Rank, Integer>> duplicateCounts) {
        return isCountMatch(duplicateCounts, 2, 2);
    }

    protected static boolean isThreeOfAKind(List<Tuple<Rank, Integer>> duplicateCounts) {
        return isCountMatch(duplicateCounts, 3);
    }

    protected static boolean isFullHouse(List<Tuple<Rank, Integer>> duplicateCounts) {
        return isCountMatch(duplicateCounts, 3, 2) || isCountMatch(duplicateCounts, 3, 3);
    }

    protected static boolean isFourOfAKind(List<Tuple<Rank, Integer>> duplicateCounts) {
        return isCountMatch(duplicateCounts, 4);
    }

    protected static boolean isFlush(Card[] cards) {
        return calculateFlush(cards) != null;
    }

    protected static boolean isStraight(Card[] cards) {
        return calculateStraight(cards) != null;
    }

    /**
     * Checks if the duplicate counts in `duplicateCounts` are equal to the
     * `expectedCounts`.
     * 
     * This function is used to calculate any type of hands where the hand contains
     * "duplicate" ranks (ex. pair, full house etc.). See {@link #countDuplicates(Card[])}
     * for the structure of the given `duplicateCounts`.
     * 
     * For example if the first item(/rank) in the given `duplicateCounts` list
     * has a count of two and the second item in the list has a count of one, this
     * is a pair. This can be deduced because there are only two cards with the
     * same rank (represented by the first item in the list with a count of two).
     * Since the second item(/rank) has a count of one, and the list is sorted by
     * count, all other ranks/cards in the list only appears once in the hand.
     *  
     * @param duplicateCounts the counts of rank duplicates ordered by count.
     * @param expectedCounts the expected counts to match against.
     * @return if the `expectedCounts` matched the actual counts.
     * @see #countDuplicates(Card[])
     */
    private static boolean isCountMatch(List<Tuple<Rank, Integer>> duplicateCounts,
                                        Integer... expectedCounts)
    {
        if (duplicateCounts.size() < expectedCounts.length) {
            return false;
        }

        boolean isMatch = true;

        for (int i = 0; i < expectedCounts.length; i++) {
            int expectedCount = expectedCounts[i];
            int actualCount = duplicateCounts.get(i).second;

            if (expectedCount != actualCount) {
                isMatch = false;
                break;
            }
        }

        return isMatch;
    }

    /**
     * Checks if the given `cards` contains a flush. If that is the case, the
     * 5 highest ranks of the flush is returned. If no flush can be found,
     * null is returned.
     * 
     * @param cards an array of Cards sorted by their ranks.
     * @return the highest ranks of the flush or null if no flush.
     */
    private static Rank[] calculateFlush(Card[] cards) {
        boolean isFlush = false;
        Suit flushSuit = null;

        Map<Suit, Integer> suitCounts = countSuit(cards);

        // See if the cards given contains a flush. If that is the case `isFlush`
        // will be set to true and the suit will be set in the `flushSuit` variable.
        for (Entry<Suit, Integer> entry : suitCounts.entrySet()) {
            flushSuit = entry.getKey();
            Integer flushCount = entry.getValue();

            if (flushCount >= 5) {
                isFlush = true;
                break;
            }
        }

        Rank[] highestRanks = null;

        // If this is a flush, find the five highest cards so that they can be
        // compared to another players ranks of the same flush. Since `cards`
        // are already sorted when this function is called, just get the five
        // first ranks of the flush suit.
        if (isFlush) {
            highestRanks = new Rank[5];

            int i = 0;
            for (Card card : cards) {
                Suit suit = card.getSuit();

                if (suit == flushSuit) {
                    highestRanks[i] = card.getRank();
                    i++;

                    if (i >= 5) {
                        break;
                    }
                }
            }
        }

        return highestRanks;
    }

    // TODO: Need to calculate straight for Ace as both higest and lowest.
    /**
     * Checks if the given `cards` creates a straight. If that is the case,
     * the rank of the card with the highest rank in the straight will be
     * returned. If no straight can be found, null is returned.
     * 
     * @param cards an array of Cards sorted by their ranks.
     * @return the highest rank of the straight or null if no straight.
     */
    private static Rank calculateStraight(Card[] cards) {
        int straightCount = 1;
        boolean isStraight = false;
        Rank highestRank = null;

        for (int i = 0; i < cards.length - 1; i++) {
            Card curr = cards[i];
            Card next = cards[i + 1];

            if (curr.getRank().ordinal() == next.getRank().ordinal() + 1) {
                if (straightCount == 1) {
                    // This is the first card in the potential straight. Since
                    // `cards` are sorted when this function is called, this
                    // rank will be the highest of the straight.
                    highestRank = curr.getRank();
                }

                straightCount++;
            } else {
                straightCount = 1;
            }

            if (straightCount == 5) {
                isStraight = true;
                break;
            }
        }

        return (isStraight) ? highestRank : null;
    }

    /**
     * Creates an array of Cards from the given community cards and hand.
     * The returned array is sorted by the rank of the cards.
     * 
     * @param communityCards
     * @param hand
     * @return an array of the cards sorted by their rank. 
     */
    private static final Card[] buildSortedCards(CommunityCards communityCards, Hand hand) {
        Card[] communityCardsArr = Arrays.copyOf(communityCards.getCards(), communityCards.getCurrentNumberOfCards());
        Card[] handCards = { hand.getFirstCard(), hand.getSecondCard() };

        Card[] cards = new Card[communityCardsArr.length + handCards.length];

        System.arraycopy(communityCardsArr, 0, cards, 0, communityCardsArr.length);
        System.arraycopy(handCards, 0, cards, communityCardsArr.length, handCards.length);

        // Only sort by rank, doesn't care about suit.
        Arrays.sort(cards, (a, b) -> {
            return (a.getRank().ordinal() > b.getRank().ordinal()) ? -1 : 1;
        });

        return cards;
    }

    /**
     * Creates a list of tuples linking a specific rank in the given `cards` to
     * the amount of cards in the hand that has that rank.
     * 
     * The resulting list is sorted by the count (Integer part of the tuple) that
     * is the count of how many cards of that rank was found in the `cards`.
     * If two items in the list has the same count, it is sorted by the rank.
     * 
     * For example if the given `cards` had the ranks:
     *   "Ace", "Three", "Eight", "Eight", "Six", "Six", "Six"
     * the resulting list would look like this:
     *   { <"Six", 3>, <"Eight", 2>, <"Ace", 1>, <"Three", 1> }
     * 
     * In the example above "Six" is first in the list because it is found 3
     * times in the cards. "Ace" comes before "Three" because they have the same
     * count but the rank "Ace" has precedence over "Three". 
     * 
     * @param cards an array of Cards sorted by their ranks.
     * @return the count of duplicates in the given `cards`.
     */
    private static List<Tuple<Rank, Integer>> countDuplicates(Card[] cards) {
        List<Tuple<Rank, Integer>> duplicateCounts = new ArrayList<>();

        int count = 1;

        for (int i = 0; i < cards.length; i++) {
            // If `curr` is the last element of the array, next will be set to null.
            Card curr = cards[i];
            Card next = (i == cards.length - 1) ? null : cards[i + 1];

            if (next != null && curr.getRank() == next.getRank()) {
                count++;
            } else {
                Tuple<Rank, Integer> countTuple = new Tuple<>(curr.getRank(), count);
                duplicateCounts.add(countTuple);
                count = 1;
            }
        }

        // Sort the list by the count of the rank (how many cards with this specific
        // rank exists in `cards`). If the count are equal, compare the rank and
        // put the higher rank cards earlier in the list. 
        duplicateCounts.sort((a, b) -> {
            Rank aRank = a.first;
            int aCount = a.second;
            Rank bRank = b.first;
            int bCount = b.second;

            if (aCount != bCount) {
                return (aCount > bCount) ? -1 : 1;
            } else if (aRank != bRank) {
                return (aRank.ordinal() > bRank.ordinal()) ? -1 : 1;
            } else {
                return 0;
            }
        });

        return duplicateCounts;
    }

    /**
     * Creates a map that keeps track of the count of the suits in the hand.
     * 
     * The map will have four entries, one for every suit. The Integer value in
     * the map will be the amount of cards with that specific suit in the given
     * `cards` hand.
     * 
     * @param cards an array of Cards.
    * @return the count of the suits found in the given `cards`.
     */
    private static Map<Suit, Integer> countSuit(Card[] cards) {
        Map<Suit, Integer> suitCounts = new HashMap<>(6);

        for (Card card : cards) {
            Suit suit = card.getSuit();

            Integer suitCount = suitCounts.get(suit);
            if (suitCount == null) {
                suitCount = 0;
            }

            suitCount++;
            suitCounts.put(suit, suitCount);
        }

        return suitCounts;
    }
}
