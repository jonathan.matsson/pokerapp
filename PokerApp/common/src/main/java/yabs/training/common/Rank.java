package yabs.training.common;

/**
 * Represents the rank of a card.
 */
public enum Rank {
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    TEN("10"),
    JACK("J"),
    QUEEN("Q"),
    KING("K"),
    ACE("A");

    /**
     * Textual representation of the rank.
     */
    private final String text;

    private Rank(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
