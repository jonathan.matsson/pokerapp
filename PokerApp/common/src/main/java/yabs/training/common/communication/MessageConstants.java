package yabs.training.common.communication;

/**
 * Constants used in messages sent between server and client.
 */
public class MessageConstants {
    // Top level fields.
    public static final String ID      = "id";
    public static final String CONTENT = "content";

    // Fields found inside "content".
    public static final String AMOUNT     = "amount";
    public static final String USER_ID    = "userId";
    public static final String TIMESTAMP  = "timestamp";
    public static final String WALLET     = "wallet";
    public static final String POSITION   = "position";
    public static final String MESSAGE    = "message";
    public static final String GAME_STATE = "gameState";
    public static final String HAND       = "hand";
    public static final String CARD       = "card";
}
