package yabs.training.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GameState implements Serializable {

    // The maximum number of players allowed to sit at the table
    private final int maxPlayerNumber;

    // The fixed small blind amount
    private final int smallBlindAmount;

    // The fixed big blind amount
    private final int bigBlindAmount;

    // A fixed size list, containing the players currently in the game.
    // Unfilled indeces in the list represent unfilled seats at the table.
    private Player[] players;
    // A set to keep track of IDs of players that have folded.
    private Set<String> foldedPlayers;
    // A map to map each playerID to the current bet by that player
    private Map<String, Integer> currentBets;

    private CommunityCards communityCards;

    // The current pot
    private int pot;

    // The currently highest live bet
    private int highestLiveBet;

    // The index in the players list of the player holding the button
    private int buttonHolderIndex;
    // The player next to the button is the small blind, the second next player is the big blind
    private int smallBlindIndex;
    // The player holding the button acts last
    private int bigBlindIndex;
    // The player who currently has the turn
    private int currentPlayerIndex;

    private int currentNumberOfPlayers;

    public GameState(int maxPlayerNumber, int smallBlindAmount, int bigBlindAmount) {
        this.maxPlayerNumber = maxPlayerNumber;
        this.smallBlindAmount = smallBlindAmount;
        this.bigBlindAmount = bigBlindAmount;
        this.pot = 0;
        this.highestLiveBet = 0;
        this.players = new Player[this.maxPlayerNumber];
        this.currentBets = new HashMap<>(this.maxPlayerNumber);
        this.foldedPlayers = new HashSet<>(this.maxPlayerNumber);
        this.communityCards = new CommunityCards();
        this.buttonHolderIndex = -1;
        this.currentPlayerIndex = -1;
        this.currentNumberOfPlayers = 0;
    }

    /**
     * Sets the game state up for a new round.
     */
    public void reset() {
        this.highestLiveBet = 0;
        this.emptyPot();
        this.foldedPlayers.clear();
        this.currentBets.clear();
        this.communityCards = new CommunityCards();
        this.moveButtonToNextPlayer();

        for (Player player : this.players) {
            if (player != null) {
                player.setHand(null);
            }
        }
    }

    public int getMaxPlayerNumber() {
        return this.maxPlayerNumber;
    }

    public int getSmallBlindAmount() {
        return this.smallBlindAmount;
    }

    public int getBigBlindAmount() {
        return this.bigBlindAmount;
    }

    public int getCurrentNumberOfPlayers() {
        return this.currentNumberOfPlayers;
    }

    public int getPot() {
        return this.pot;
    }

    public int getButtonHolderIndex() {
        return this.buttonHolderIndex;
    }

    public int getSmallBlindIndex() {
        return this.smallBlindIndex;
    }

    public int getBigBlindIndex() {
        return this.bigBlindIndex;
    }

    public CommunityCards getCommunityCards() {
        return this.communityCards;
    }

    /**
     * @return A list containing the players, including the empty seats.
     */
    public Player[] getPlayers() {
        return this.players;
    }

    /**
     * @return A list containing only the players actually sitting at the table
     */
    public Player[] getSittingPlayers() {
        List<Player> sittingPlayers = new ArrayList<>();

        for(Player player : this.players) {
            if(player != null) {
                sittingPlayers.add(player);
            }
        }

        return sittingPlayers.toArray(new Player[0]);
    }

    /**
     * Searches for a player with the given User ID in the currently sitting players
     * @param playerID The User ID of the player to search for.
     * @return The player with the given User ID. Null if no such player was found.
     */
    public Player getPlayer(String playerID) {
        Player requestedPlayer = null;
        int index = 0;

        while(requestedPlayer == null && index < this.maxPlayerNumber) {
            Player player = this.players[index];

            if(player != null && playerID.equals(player.getUserID())) {
                requestedPlayer = player;
            }

            index = index + 1;
        }

        return requestedPlayer;
    }

    /**
     * @param playerID ID of the player to check.
     * @return True if the player with the given ID has folded, false otherwise.
     */
    public boolean hasPlayerFolded(String playerID) {
        return this.foldedPlayers.contains(playerID);
    }

    /**
     * @param playerID ID of the player to get the bet for.
     * @return The current amount bet by the player with the given ID.
     */
    public int getPlayerBet(String playerID) {
        Integer bet = this.currentBets.get(playerID);
        return (bet != null) ? bet : 0;
    }

    /**
     * @return The highest live bet made during the current round.
     */
    public int getHighestLiveBet() {
        return this.highestLiveBet;
    }

    /**
     * @return The player ID of the player who has the current turn.
     */
    public String getCurrentPlayerID() {
        if(this.currentPlayerIndex < 0 || this.currentPlayerIndex >= maxPlayerNumber) {
            return null;
        }

        return this.players[this.currentPlayerIndex].getUserID();
    }

    /**
     * @return The index of the player who has the current turn.
     */
    public int getCurrentPlayerIndex() {
        return this.currentPlayerIndex;
    }

    /**
     * Sets the bet amount for a player.
     * @param playerID ID of the player to get the bet for.
     */
    public void setPlayerBet(String playerID, int amount) {
        this.currentBets.put(playerID, amount);
        // If this is the new highest live bet, update the highest live bet value
        if(amount > this.highestLiveBet) {
            this.highestLiveBet = amount;
        }
    }

    /**
     * Raises the current bet for a player by a given amount.
     * @param playerID ID of the player to get the bet for.
     */
    public void raisePlayerBet(String playerID, int amount) {
        Integer currentBet = currentBets.get(playerID);
        int bet = (currentBet != null) ? currentBet : 0;
        bet = bet + amount;

        this.setPlayerBet(playerID, bet);
    }

    /**
     * Increases the current pot by the given amount.
     * @param amount The amount to add to the current pot.
     */
    public void increasePot(int amount) {
        this.pot = this.pot + amount;
    }

    /**
     * Empties the pot by setting it back to zero.
     */
    public void emptyPot() {
        this.pot = 0;
    }

    /**
     * Adds a new community card
     * @param newCard The card to be added.
     */
    public void addCommunityCard(Card newCard) {
        this.communityCards.addCard(newCard);
    }

    /**
     * Adds a player to the table at the given index.
     * @param index
     * @param player
     */
    public void addPlayer(int index, Player player) {
        if(this.players[index] == null) {
            this.players[index] = player;
            this.currentNumberOfPlayers = this.currentNumberOfPlayers + 1;
        } else {
            // TODO: How should we handle this? Throw an exception? Insert in next available spot?
        }

    }

    public void foldPlayer(String playerID) {
        this.foldedPlayers.add(playerID);
    }

    /**
     * Removes a player with the given ID from the table.
     * @param playerID The userID of the player to be removed.
     */
    public void removePlayer(String playerID) {
        boolean playerRemoved = false;
        int index = 0;

        while(!playerRemoved && index < this.maxPlayerNumber) {
            Player player = this.players[index];

            if(player != null && playerID.equals(player.getUserID())) {
                this.players[index] = null;
                this.currentNumberOfPlayers = this.currentNumberOfPlayers - 1;
                playerRemoved = true;
            }

            index = index + 1;
        }
    }

    /**
     * Gives the turn to the next player.
     */
    public void moveToNextPlayer() {
        this.currentPlayerIndex = this.getNextPlayerIndex(this.currentPlayerIndex);
    }

    /**
     * Gives the turn to the next player that hasn't folded.
     */
    public void moveToNextActivePlayer() {
        this.currentPlayerIndex = this.getNextActivePlayerIndex(this.currentPlayerIndex);
    }

    /**
     * Moves the button to the next player sitting at the table.
     * The small and big blinds and next player to play are also moved.
     */
    public void moveButtonToNextPlayer() {
        this.buttonHolderIndex = this.getNextPlayerIndex(this.buttonHolderIndex);
        Log.debug("Button moved to player at index: " + this.buttonHolderIndex);

        if(this.currentNumberOfPlayers == 2) {
            this.smallBlindIndex = this.buttonHolderIndex;
        } else {
            this.smallBlindIndex = this.getNextPlayerIndex(this.buttonHolderIndex);
        }
        Log.debug("Small blind moved to player at index: " + this.smallBlindIndex);


        this.bigBlindIndex = this.getNextPlayerIndex(this.smallBlindIndex);
        Log.debug("Big blind moved to player at index: " + this.bigBlindIndex);


        // The player after big blind is the first to play
        this.currentPlayerIndex = this.getNextPlayerIndex(this.bigBlindIndex);
        Log.debug("Current player index: " + this.currentPlayerIndex);

    }

    /**
     * @param startIndex The index to start searching from, excluded from the search
     * @return the index of the next filled seat in the players list
     */
    public int getNextPlayerIndex(int startIndex) {
        if (this.currentNumberOfPlayers == 0) {
            return -1;
        }

        int nextPlayerIndex = -1;
        int index;

        int playersSize = this.maxPlayerNumber;

        if (startIndex >= 0 && startIndex < playersSize) {
            // If a valid index was given, start searching from the next index
            index = startIndex + 1;
        } else {
            // If an invalid index or last possible index (size - 1) was given, start searching from 0
            startIndex = 0;
            index = 0;
        }

        boolean nextPlayerFound = false;

         do {
            Player player = this.players[index];

            if(player != null) {
                nextPlayerFound = true;
                nextPlayerIndex = index;
            }

            if(index == (playersSize - 1)) {
                index = 0;
            } else {
                index = index + 1;
            }
        } while (!nextPlayerFound && index != startIndex);

        return nextPlayerIndex;
    }

    /**
     * @param startIndex The index to start searching from, excluded from the search
     * @return the index of the next filled seat in the players list, containing a player that is still active.
     */
    public int getNextActivePlayerIndex(int startIndex) {
        if (this.currentNumberOfPlayers == 0) {
            return -1;
        }

        int nextActivePlayerIndex = -1;
        int index = this.getNextPlayerIndex(startIndex);

        boolean nextActivePlayerFound = false;

        do {
            Player player = this.players[index];

            if(player != null && !hasPlayerFolded(player.getUserID())) {
                nextActivePlayerFound = true;
                nextActivePlayerIndex = index;
            } else {
                index = this.getNextPlayerIndex(index);
            }
        } while(!nextActivePlayerFound && index != startIndex);

        return nextActivePlayerIndex;
    }

    public Player[] getPlayersNotFolded() {
        Set<Integer> notFoldedPlayerIndices = this.getPlayerIndicesNotFolded();
        Player[] notFoldedPlayers = new Player[notFoldedPlayerIndices.size()];
        
        int i = 0;
        for (Integer index : notFoldedPlayerIndices) {
            Player player = this.players[index];
            notFoldedPlayers[i++] = player;
        }

        return notFoldedPlayers;
    } 

    public Set<Integer> getPlayerIndicesNotFolded() {
        Set<Integer> notFoldedPlayers = new HashSet<>();

        Player[] players = this.getPlayers();
        for (int i = 0; i < players.length; i++) {
            Player player = players[i];
            if (player != null && !hasPlayerFolded(player.getUserID())) {
                notFoldedPlayers.add(i);
            }
        }

        return notFoldedPlayers;
    } 

    public int getFirstVacantSpot() {
        int emptyIndex = -1;
        int index = 0;
        boolean spotFound = false;

        while(!spotFound && index < this.maxPlayerNumber) {
            Player player = this.players[index];

            if(player == null) {
                spotFound = true;
                emptyIndex = index;
            }

            index = index + 1;
        }

        return emptyIndex;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Current Game State");

        CommunityCards communityCards = this.getCommunityCards();
        int nrOfCards = communityCards.getCurrentNumberOfCards();
        sb.append("\n  Cards on table: ");
        if (nrOfCards > 0) {
            Card[] cards = communityCards.getCards();
            for (int i = 0; i < nrOfCards; i++) {
                sb.append(cards[i] + " ");
            }
        } else {
            sb.append("No cards on the table.");
        }

        sb.append("\n  Pot: " + this.getPot());
        sb.append("\n  Player to act: " + this.getCurrentPlayerID());
        sb.append("\n  Players: ");
        for (Player player : this.getSittingPlayers()) {
            String userId = player.getUserID();

            sb.append("\n   - Player \"" + userId + "\"");
            sb.append(" - Wallet: " + player.getWallet());
            sb.append(", current bet: ");
            if (this.hasPlayerFolded(userId)) {
                sb.append("FOLDED");
            } else {
                Integer betAmount = this.currentBets.get(userId);
                betAmount = betAmount != null ? betAmount : 0;

                sb.append(betAmount);
            }
        }

        return sb.toString();
    }
}

