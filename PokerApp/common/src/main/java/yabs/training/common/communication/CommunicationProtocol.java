package yabs.training.common.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import yabs.training.common.Card;
import yabs.training.common.GameState;
import yabs.training.common.Hand;

/**
 * A class used to create messages that is to be sent between server and client.
 * See "Messages.docx" in team work space for more information.
 */
public class CommunicationProtocol {
    /**
     * The max allowed length of data sent over a stream. If a message length
     * exceeds this value, a exception will be thrown.
     */
    public static final int MAX_MSG_LENGTH = 65536;

    /**
     * Sends the given message `msg` to the output stream `output`. This function
     * will make sure that the message is formatted so that it can be received
     * on the other end of the stream correctly.
     * 
     * The corresponding function `receiveMessage()` should be used to read messages
     * that is sent with this function. Only these two functions should know and
     * implement the exact format for the raw data sent over the stream.
     * 
     * @param output a OutputStream to send the message over.
     * @param msg the message to send.
     * @throws IOException if unable to send the message.
     */
    public static void sendMessage(OutputStream output, Message msg) throws IOException {
        byte[] jsonBytes = msg.asJsonBytes();

        int msgLength = jsonBytes.length;
        if (msgLength > MAX_MSG_LENGTH) {
            String errMsg = "Tried to send a message that is to long.";
            errMsg += " Max allowed length: " + MAX_MSG_LENGTH + ", actual: " + msgLength + ".";
            throw new IllegalStateException(errMsg);
        }

        // Add the length of the message as the first 4 bytes of the data that is
        // sent over the stream so that the receiver knows when the whole message
        // has been sent.
        byte[] data = ByteBuffer.allocate(4 + msgLength)
            .putInt(msgLength)
            .put(jsonBytes)
            .array();

        output.write(data);
    }

    // TODO: Add timeout to read. Can get stuck reading a message forever. 
    /**
     * Reads a message from the given input stream `input`.
     * 
     * The data to read from the stream is expected to have a format according
     * to a `sendMessage()` call.
     * 
     * @param input a InputStream to read a message from.
     * @return the read message.
     * @throws IOException if unable to read a message.
     */
    public static Message readMessage(InputStream input) throws IOException {
        byte[] msgLengthBuf = new byte[4];

        // Read in the first four bytes from the stream which should contain the
        // length of the message.
        for (int i = 0; i < msgLengthBuf.length; i++) {
            int b = input.read();
            if (b == -1) {
                String errMsg = "Got EOF when reading length of message.";
                throw new IllegalStateException(errMsg);
            }

            msgLengthBuf[i] = (byte)b;
        }

        int msgLength = ByteBuffer.wrap(msgLengthBuf).getInt();
        if (msgLength > MAX_MSG_LENGTH) {
            String errMsg = "Received a message that is to long.";
            errMsg += " Max allowed length: " + MAX_MSG_LENGTH + ", got: " + msgLength + ".";
            throw new IllegalStateException(errMsg);
        }

        byte[] msgData = new byte[msgLength];

        // Read in the actual json message from the stream.
        int index = 0;
        int remainingLength = msgLength;
        while (remainingLength > 0) {
            int readLength = input.read(msgData, index, remainingLength);
            if (readLength == -1) {
                String errMsg = "Got EOF when reading message.";
                errMsg += " Message length: " + msgLength + ", read so far: " + index;
                throw new IllegalStateException(errMsg);
            }

            index += readLength;
            remainingLength -= readLength;
        }

        return CommunicationProtocol.parseMessage(msgData);
    }


    /**
     * Parses the contents of the byte array `data` into a Message object.
     * 
     * The byte array should have been created with a `asJsonBytes()` on a
     * Message object. This function will reverse that logic to re-create the
     * message.
     * 
     * @param data a byte array representing a Message.
     * @return the Message represented by the `data` byte array.
     */
    public static Message parseMessage(byte[] data) {
        String dataStr = new String(data, StandardCharsets.UTF_8);
        JsonObject jsonMsg = JsonParser.parseString(dataStr).getAsJsonObject();

        String msgId = JsonUtil.getString(jsonMsg, MessageConstants.ID);

        if (MessageId.parseId(msgId) == MessageId.GameStateResponse) {
            return new GameStateMessage(jsonMsg);
        } else {
            return new Message(jsonMsg);
        }
    }

    /**
     * The functions underneath are helper functions used to create byte arrays
     * representing messages that can be sent over the socket.
     */

    public static Message gameStateRequest(long timestamp) {
        return new Message(MessageId.GameStateRequest)
            .set(MessageConstants.TIMESTAMP, Long.toString(timestamp));
    }

    public static GameStateMessage gameStateResponse(GameState gameState, long timestamp) {
        GameStateMessage gameStateMessage = new GameStateMessage(MessageId.GameStateResponse, gameState);
        gameStateMessage.set(MessageConstants.TIMESTAMP, Long.toString(timestamp));

        return gameStateMessage;
    }

    public static Message gameStateUpdateBet(String userId, int amount) {
        return new Message(MessageId.GameStateUpdateBet)
            .set(MessageConstants.USER_ID, userId)
            .set(MessageConstants.AMOUNT, Integer.toString(amount));
    }

    public static Message gameStateUpdateFold(String userId) {
        return new Message(MessageId.GameStateUpdateFold)
            .set(MessageConstants.USER_ID, userId);
    }

    public static Message gameStateUpdateCheck(String userId) {
        return new Message(MessageId.GameStateUpdateCheck)
            .set(MessageConstants.USER_ID, userId);
    }

    public static Message gameStateUpdateHandOutcome(String userId, Hand hand) {
        return new Message(MessageId.GameStateUpdateHandOutcome)
            .set(MessageConstants.USER_ID, userId)
            .set(MessageConstants.HAND, hand);
    }

    public static Message gameStateUpdateJoin(String userId, int wallet, int position) {
        return new Message(MessageId.GameStateUpdateJoin)
            .set(MessageConstants.USER_ID, userId)
            .set(MessageConstants.WALLET, Integer.toString(wallet))
            .set(MessageConstants.POSITION, Integer.toString(position));
    }

    public static Message gameStateUpdateLeave(String userId) {
        return new Message(MessageId.GameStateUpdateLeave)
            .set(MessageConstants.USER_ID, userId);
    }

    public static Message gameStateUpdateNewCard(Card card) {
        return new Message(MessageId.GameStateUpdateNewCard)
            .set(MessageConstants.CARD, card);
    }

    public static Message gameStateUpdateNewHand(Hand hand) {
        return new Message(MessageId.GameStateUpdateNewHand)
            .set(MessageConstants.HAND, hand);
    }

    public static Message disconnect() {
        return new Message(MessageId.Disconnect);
    }

    public static Message actionRequest(long timestamp) {
        return new Message(MessageId.ActionRequest)
            .set(MessageConstants.TIMESTAMP, Long.toString(timestamp));
    }

    public static Message actionResponseBet(long timestamp, int amount) {
        return new Message(MessageId.ActionResponseBet)
            .set(MessageConstants.TIMESTAMP, Long.toString(timestamp))
            .set(MessageConstants.AMOUNT, Integer.toString(amount));
    }

    public static Message actionResponseFold(long timestamp) {
        return new Message(MessageId.ActionResponseFold)
            .set(MessageConstants.TIMESTAMP, Long.toString(timestamp));
    }

    public static Message actionResponseCheck(long timestamp) {
        return new Message(MessageId.ActionResponseCheck)
            .set(MessageConstants.TIMESTAMP, Long.toString(timestamp));
    }

    public static Message textMessage(String msg) {
        return new Message(MessageId.TextMessage)
            .set(MessageConstants.MESSAGE, msg);
    }

    public static Message textMessageBroadCast(String userId, String msg) {
        return new Message(MessageId.TextMessageBroadCast)
            .set(MessageConstants.USER_ID, userId)
            .set(MessageConstants.MESSAGE, msg);
    }

    public static Message connected(String userId) {
        return new Message(MessageId.Connected)
            .set(MessageConstants.USER_ID, userId);
    }

    public static Message error(String message, long timestamp) {
        return new Message(MessageId.Error)
                .set(MessageConstants.MESSAGE, message)
                .set(MessageConstants.TIMESTAMP, timestamp);
    }
}
