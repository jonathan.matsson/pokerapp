package yabs.training.common.communication;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import yabs.training.common.GameState;

/**
 * A Message that contains a GameState. This is only used when sending a
 * "GameStateResponse". Currently this is the only message that are different
 * from a "regular" message. Is there any other easy solution that doesn't
 * require a custom class that extends Message? 
 */
public class GameStateMessage extends Message {
    // TODO: Filter out the hands for other players. Ex. if player 1 request
    //       the current game state, it should not see what hand player 2 has.
    public GameStateMessage(MessageId id, GameState gameState) {
        super(id);

        JsonElement gameStateElem = new Gson().toJsonTree(gameState);
        Object gameStateObj = jsonToJava(gameStateElem);

        super.set(MessageConstants.GAME_STATE, gameStateObj);
    }

    public GameStateMessage(JsonElement jsonMsg) {
        super(jsonMsg);
    }

    /**
     * Parses and return the "GameState" instance from the message content.
     */
    public GameState getGameState() {
        JsonObject obj = super.asJson().getAsJsonObject();

        JsonObject content = JsonUtil.getObject(obj, MessageConstants.CONTENT);
        JsonObject gameState = JsonUtil.getObject(content, MessageConstants.GAME_STATE);

        return new Gson().fromJson(gameState, GameState.class);
    }
}
