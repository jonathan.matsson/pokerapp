package yabs.training.common.communication;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Utility functions for common operations on gson's Json objects.
 */
class JsonUtil {
    protected static JsonElement getElement(JsonObject obj, String name) {
        if (obj == null) {
            throw new IllegalArgumentException("Parameter \"obj\" is null.");
        } else if (name == null) {
            throw new IllegalArgumentException("Parameter \"name\" is null.");
        }

        JsonElement value = obj.get(name);
        return (value != null && !value.isJsonNull()) ? value : null;
    }

    protected static String getString(JsonObject obj, String name) {
        JsonElement value = JsonUtil.getElement(obj, name);
        if(value == null) {
            return null;
        }

        if (!value.isJsonPrimitive() || !value.getAsJsonPrimitive().isString()) {
            String msg = "Expected field with name \"" + name + "\" to be String, but wasn't.";
            throw new IllegalStateException(msg);
        }
        return value.getAsString();
    }

    protected static JsonObject getObject(JsonObject obj, String name) {
        JsonElement value = obj.get(name);
        if (!value.isJsonObject()) {
            String msg = "Expected field with name \"" + name + "\" to be JsonObject, but wasn't.";
            throw new IllegalStateException(msg);
        }
        return value.getAsJsonObject();
    }
}
