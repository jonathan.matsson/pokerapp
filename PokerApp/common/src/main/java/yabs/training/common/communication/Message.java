package yabs.training.common.communication;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import yabs.training.common.Card;
import yabs.training.common.GameState;
import yabs.training.common.Hand;

/**
 * Represents a message that can be sent between server and client.
 */
public class Message {
    private final MessageId id;
    private final Map<String, Object> content;

    public Message(MessageId id) {
        this.id = id;
        this.content = new HashMap<>();
    }

    /**
     * Creates a Message object from a JSON message.
     */
    public Message(JsonElement jsonMsg) {
        JsonObject jsonObj = jsonMsg.getAsJsonObject();

        String msgId = JsonUtil.getString(jsonObj, MessageConstants.ID);
        this.id = MessageId.parseId(msgId);

        this.content = new HashMap<>();

        // Populate `this.content` with the key-value pairs found inside the
        // "content" field of the json message.
        JsonObject jsonContent = JsonUtil.getObject(jsonObj, MessageConstants.CONTENT);
        this.content.putAll((Map<String, Object>)jsonToJava(jsonContent));
    }

    /**
     * Sets a key value pair in the message. Returns `this` for chaining of calls.
     */
    public Message set(String key, Object value) {
        this.content.put(key, value);
        return this;
    }

    /**
     * Gets the value for the item with key `key`.
     */
    public Object get(String key) {
        return this.content.get(key);
    }

    public MessageId getId() {
        return this.id;
    }

    public long getLong(String key) {
        Object value = this.get(key);
        if (value instanceof String) {
            return Long.valueOf((String)value);
        } else if (value instanceof Long) {
            return (Long)value;
        } else {
            String errMsg = "Unable to parse incorrect " + key + "(long): " + value;
            throw new IllegalStateException(errMsg);
        }
    }

    public String getString(String key) {
        Object value = this.get(key);
        if (!(value instanceof String)) {
            String errMsg = "Unable to parse incorrect " + key + "(String): " + value;
            throw new IllegalStateException(errMsg);
        }
        return (String)value;
    }

    public int getInt(String key) {
        Object value = this.get(key);
        if (value instanceof String) {
            return Integer.valueOf((String)value);
        } else if (value instanceof Integer) {
            return (Integer)value;
        } else {
            String errMsg = "Unable to parse incorrect " + key + "(int): " + value;
            throw new IllegalStateException(errMsg);
        }
    }

    public Hand getHand() {
        Object hand = this.get(MessageConstants.HAND);
        if (!(hand instanceof Hand)) {
            String errMsg = "Unable to parse incorrect hand: " + hand;
            throw new IllegalStateException(errMsg);
        }
        return (Hand)hand;
    }

    /**
     * Converts `this` message into an array of bytes. This is the format of
     * the message that should be sent over the socket.
     * 
     * The message will be formatted as json before being turned into bytes.
     * The encoding is UTF-8.
     */
    public byte[] asJsonBytes() {
        JsonElement jsonMsg = this.asJson();
        return new Gson().toJson(jsonMsg).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Converts `this` message into a json element.
     */
    protected JsonElement asJson() {
        JsonObject jsonMsg = new JsonObject();
        JsonObject jsonMsgContent = new JsonObject();
        Gson gson = new Gson();

        jsonMsg.addProperty(MessageConstants.ID, this.id.toString());
        jsonMsg.add(MessageConstants.CONTENT, jsonMsgContent);

        for (Entry<String, Object> entry : this.content.entrySet()) {
            JsonElement valueJson = gson.toJsonTree(entry.getValue());
            jsonMsgContent.add(entry.getKey(), valueJson);
        }

        return jsonMsg;
    }

    /**
     * Converts a JsonElement into Java objects.
     * The values are convert from Json to Java as follows:
     *   JsonObject     =>  Map
     *   JsonArray      =>  List
     *   JsonPrimitive  =>  Java primitive
     *   JsonNull       =>  null
     */
    protected static Object jsonToJava(JsonElement elem) {
        if (elem.isJsonObject()) {

            Map<String, Object> map = new HashMap<>();
            JsonObject obj = elem.getAsJsonObject();
            for (Entry<String, JsonElement> entry : obj.entrySet()) {
                Gson gson = new Gson();

                // To allow for serializing/deserializing of custom classes with
                // Json, this switch is used to match on known field keys in the
                // message and then parse them into the correct class.
                //
                // Any primitive/String can be handled by the Json framework
                // by default, so no custom logic is requred for them.
                Object value;
                switch (entry.getKey()) {
                    case MessageConstants.GAME_STATE:
                        value = gson.fromJson(entry.getValue(), GameState.class);
                        break;
                    case MessageConstants.HAND:
                        value = gson.fromJson(entry.getValue(), Hand.class);
                        break;
                    case MessageConstants.CARD:
                        value = gson.fromJson(entry.getValue(), Card.class);
                        break;
                    default:
                        value = jsonToJava(entry.getValue());
                        break;
                }

                map.put(entry.getKey(), value);
            }
            return map;

        } else if (elem.isJsonArray()) {

            List<Object> list = new ArrayList<>();
            JsonArray array = elem.getAsJsonArray();
            for (JsonElement item : array) {
                list.add(jsonToJava(item));
            }
            return list;

        } else if (elem.isJsonPrimitive()) {

            JsonPrimitive prim = elem.getAsJsonPrimitive();
            if (prim.isString()) {
                return prim.getAsString();
            } else if (prim.isBoolean()) {
                return prim.getAsBoolean();
            } else if (prim.isNumber()) {
                return prim.getAsLong();
            }

        } else if (elem.isJsonNull()) {

            return null;

        }

        // TODO: Exception type.
        throw new RuntimeException("Bad type of JsonElement: " + elem);
    }
}
