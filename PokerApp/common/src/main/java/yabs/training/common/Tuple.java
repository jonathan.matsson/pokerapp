package yabs.training.common;

import java.util.Objects;

/**
 * A basic pair/tuple.
 */
public class Tuple<T1, T2> {
    private final T1 first;
    private final T2 second;

    public Tuple(T1 first, T2 second) {
        this.first = first;
        this.second = second;
    }

    public T1 getFirst() {
        return this.first;
    }

    public T2 getSecond() {
        return this.second;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (!(other instanceof Tuple)) {
            return false;
        }

        @SuppressWarnings("unchecked")
        Tuple<T1, T2> o = (Tuple<T1, T2>)other;
        return this.first.equals(o.first) && this.second.equals(o.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.first, this.second);
    }
}
