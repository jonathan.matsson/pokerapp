package yabs.training.common.communication;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

import yabs.training.common.Card;
import yabs.training.common.GameState;
import yabs.training.common.Hand;
import yabs.training.common.Rank;
import yabs.training.common.Suit;

public class CommunicationProtocolTest {
    @Test
    public void creatingMessagesWorksCorrectly() {
        // ARRANGE
        long timestamp = 12345;
        String userId = "abc123";
        int amount = 67890;
        int wallet = 1;
        int position = 0;
        String msg = "hej";
        GameState gameState = new GameState(0, 0, 0);
        Card card = new Card(Suit.HEART, Rank.ACE);
        Hand hand = new Hand(card, card);

        // ACT & ASSERT
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateRequest(timestamp));
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateResponse(gameState, timestamp));
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateUpdateBet(userId, amount));
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateUpdateFold(userId));
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateUpdateCheck(userId));
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateUpdateHandOutcome(userId, hand));
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateUpdateJoin(userId, wallet, position));
        assertDoesNotThrow(() -> CommunicationProtocol.gameStateUpdateLeave(userId));
        assertDoesNotThrow(() -> CommunicationProtocol.disconnect());
        assertDoesNotThrow(() -> CommunicationProtocol.actionRequest(timestamp));
        assertDoesNotThrow(() -> CommunicationProtocol.actionResponseBet(timestamp, amount));
        assertDoesNotThrow(() -> CommunicationProtocol.actionResponseFold(timestamp));
        assertDoesNotThrow(() -> CommunicationProtocol.actionResponseCheck(timestamp));
        assertDoesNotThrow(() -> CommunicationProtocol.textMessage(msg));
        assertDoesNotThrow(() -> CommunicationProtocol.textMessageBroadCast(userId, msg));
        assertDoesNotThrow(() -> CommunicationProtocol.connected(userId));
        assertDoesNotThrow(() -> CommunicationProtocol.error(msg, timestamp));
    }
}
