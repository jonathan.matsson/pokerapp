package yabs.training.common.communication;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import yabs.training.common.Card;
import yabs.training.common.CommunityCards;
import yabs.training.common.GameState;
import yabs.training.common.Hand;
import yabs.training.common.Player;
import yabs.training.common.Rank;
import yabs.training.common.Suit;

public class GameStateMessageParseTest {
    public static final long timestamp = 12345;
    public static final int maxPlayerNumber = 4;
    public static final int smallBlindAmount = 10;
    public static final int bigBlindAmount = 20;
    public static final int wallet = 15;
    public static final String userId = "USER_ID";
    public static final Card cardOne = new Card(Suit.HEART, Rank.ACE);
    public static final Card cardTwo = new Card(Suit.HEART, Rank.TWO);
    public static final Card cardThree = new Card(Suit.HEART, Rank.THREE);

    public static GameState expectedGameState;
    public static CommunityCards communityCards;
    public static Player player;
    public static Hand hand;

    public static GameState actualGameState;

    @BeforeAll
    public static void setUp() {
        // ARRANGE
        expectedGameState = new GameState(maxPlayerNumber, smallBlindAmount, bigBlindAmount);

        hand = new Hand(cardTwo, cardThree);
        player = new Player(userId, wallet);
        player.setHand(hand);

        expectedGameState.addCommunityCard(cardOne);
        expectedGameState.addPlayer(0, player);

        // ACT
        GameStateMessage message = CommunicationProtocol.gameStateResponse(expectedGameState, timestamp);
        actualGameState = message.getGameState();
    }

    @Test
    public void maxPlayerNumberParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getMaxPlayerNumber(), actualGameState.getMaxPlayerNumber());
    }
    
    @Test
    public void smallBlindAmountParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getSmallBlindAmount(), actualGameState.getSmallBlindAmount());
    }

    @Test
    public void bigBlindAmountParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getBigBlindAmount(), actualGameState.getBigBlindAmount());
    }

    @Test
    public void playersParsedCorrectly() {
        Player expectedPlayer = expectedGameState.getPlayers()[0];
        Player actualPlayer = actualGameState.getPlayers()[0];

        // ASSERT
        assertEquals(expectedGameState.getFirstVacantSpot(), actualGameState.getFirstVacantSpot());
        assertEquals(expectedPlayer.getUserID(), actualPlayer.getUserID());
        assertEquals(expectedPlayer.getWallet(), actualPlayer.getWallet());
        assertEquals(expectedPlayer.getHand().getFirstCard(), actualPlayer.getHand().getFirstCard());
        assertEquals(expectedPlayer.getHand().getSecondCard(), actualPlayer.getHand().getSecondCard());
    }

    @Test
    public void communityCardsParsedCorrectly() {
        CommunityCards expectedCommunityCards = expectedGameState.getCommunityCards();
        CommunityCards actualCommunityCards = actualGameState.getCommunityCards();

        // ASSERT
        assertEquals(expectedCommunityCards.getCurrentNumberOfCards(), actualCommunityCards.getCurrentNumberOfCards());
        assertEquals(expectedCommunityCards.getCards()[0], actualCommunityCards.getCards()[0]);
    }

    @Test
    public void potParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getPot(), actualGameState.getPot());
    }

    @Test
    public void buttonHolderIndexParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getButtonHolderIndex(), actualGameState.getButtonHolderIndex());
    }

    @Test
    public void smallBlindIndexParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getSmallBlindIndex(), actualGameState.getSmallBlindIndex());
    }

    @Test
    public void bigBlindIndexParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getBigBlindIndex(), actualGameState.getBigBlindIndex());
    }

    @Test
    public void currentNumberOfPlayersParsedCorrectly() {
        // ASSERT
        assertEquals(expectedGameState.getCurrentNumberOfPlayers(), actualGameState.getCurrentNumberOfPlayers());
    }
}
