package yabs.training.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.junit.jupiter.api.Test;

import yabs.training.common.HandCalculations.HandRank;

public class HandCalculationsTest {
    @Test
    public void nothingCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.NOTHING;
        CommunityCards communityCards = new CommunityCards();
        Hand hand;

        communityCards.addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards.addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards.addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards.addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards.addCard(new Card(Suit.HEART, Rank.TEN));
        hand = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );

        // ACT
        HandRank actualHandRank = HandCalculations.handRank(communityCards, hand);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank);
    }

    @Test
    public void pairCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.PAIR;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Pair in hand.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.TEN));
        hand[0] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Pair in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[1].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[1] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );

        // Pair in hand + community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[2].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.TWO));
        hand[2] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void twoPairCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.TWO_PAIR;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Two pair with one pair in hand and one in community cards.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[0] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Two pair with one card in hand and three in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.TWO));
        communityCards[1].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[1] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );

        // Two pair in community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.EIGHT));
        communityCards[2].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[2] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void threeOfAKindCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.THREE_OF_A_KIND;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Three of a kind with two cards in hand and one in community cards.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.TWO));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.TEN));
        hand[0] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Three of a kind with one card in hand and two in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[1].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[1] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.SEVEN)
        );

        // Three of a kind with three in community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[2].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.EIGHT));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.EIGHT));
        hand[2] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void straightCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.STRAIGHT;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Straight with two cards in hand and three in community cards.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.SIX));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.FOUR));
        hand[0] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.HEART, Rank.THREE)
        );

        // Straight with one card in hand and four in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.THREE));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.SIX));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.FOUR));
        hand[1] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.DIAMOND, Rank.EIGHT)
        );

        // Straight with five in community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.SPADE, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.THREE));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.SIX));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.FOUR));
        hand[2] = new Hand(
            new Card(Suit.CLUB, Rank.ACE),
            new Card(Suit.DIAMOND, Rank.EIGHT)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void flushCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.FLUSH;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Flush with two cards in hand and three in community cards.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.HEART, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.TEN));
        hand[0] = new Hand(
            new Card(Suit.HEART, Rank.THREE),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Flush with one card in hand and four in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.HEART, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.FIVE));
        communityCards[1].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.THREE));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.TEN));
        hand[1] = new Hand(
            new Card(Suit.CLUB, Rank.SEVEN),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Flush with five in community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.HEART, Rank.ACE));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.FIVE));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.THREE));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.TEN));
        hand[2] = new Hand(
            new Card(Suit.CLUB, Rank.SEVEN),
            new Card(Suit.DIAMOND, Rank.EIGHT)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void fullHouseCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.FULL_HOUSE;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Full house with two cards in hand and three in community cards.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.SEVEN));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[0] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Full house with one card in hand and four in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.SPADE, Rank.TWO));
        communityCards[1].addCard(new Card(Suit.DIAMOND, Rank.SEVEN));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[1] = new Hand(
            new Card(Suit.CLUB, Rank.FIVE),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Full house with five in community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.HEART, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.SPADE, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.DIAMOND, Rank.SEVEN));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.SEVEN));
        hand[2] = new Hand(
            new Card(Suit.CLUB, Rank.FIVE),
            new Card(Suit.CLUB, Rank.ACE)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void fourOfAKindCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.FOUR_OF_A_KIND;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Four of a kind with two cards in hand and two in community cards.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.TWO));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.TWO));
        hand[0] = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Four of a kind with one card in hand and three in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[1].addCard(new Card(Suit.SPADE, Rank.TWO));
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.TWO));
        communityCards[1].addCard(new Card(Suit.DIAMOND, Rank.TWO));
        hand[1] = new Hand(
            new Card(Suit.DIAMOND, Rank.EIGHT),
            new Card(Suit.HEART, Rank.TWO)
        );

        // Four of a kind with four in community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.HEART, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards[2].addCard(new Card(Suit.SPADE, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.CLUB, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.DIAMOND, Rank.TWO));
        hand[2] = new Hand(
            new Card(Suit.DIAMOND, Rank.EIGHT),
            new Card(Suit.CLUB, Rank.ACE)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void straightFlushCalculatedCorrectly() {
        // ARRANGE
        HandRank expectedHandRank = HandRank.STRAIGHT_FLUSH;
        CommunityCards[] communityCards = new CommunityCards[3];
        Hand[] hand = new Hand[3];

        // Straight flush with two cards in hand and three in community cards.
        communityCards[0] = new CommunityCards();
        communityCards[0].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.FIVE));
        communityCards[0].addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.SIX));
        communityCards[0].addCard(new Card(Suit.HEART, Rank.FOUR));
        hand[0] = new Hand(
            new Card(Suit.HEART, Rank.TWO),
            new Card(Suit.HEART, Rank.THREE)
        );

        // Straight flush with one card in hand and four in community cards.
        communityCards[1] = new CommunityCards();
        communityCards[1].addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.FIVE));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.THREE));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.SIX));
        communityCards[1].addCard(new Card(Suit.HEART, Rank.FOUR));
        hand[1] = new Hand(
            new Card(Suit.HEART, Rank.TWO),
            new Card(Suit.DIAMOND, Rank.EIGHT)
        );

        // Straight flush with five in community cards.
        communityCards[2] = new CommunityCards();
        communityCards[2].addCard(new Card(Suit.HEART, Rank.TWO));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.FIVE));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.THREE));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.SIX));
        communityCards[2].addCard(new Card(Suit.HEART, Rank.FOUR));
        hand[2] = new Hand(
            new Card(Suit.CLUB, Rank.ACE),
            new Card(Suit.DIAMOND, Rank.EIGHT)
        );

        // ACT
        HandRank actualHandRank0 = HandCalculations.handRank(communityCards[0], hand[0]);
        HandRank actualHandRank1 = HandCalculations.handRank(communityCards[1], hand[1]);
        HandRank actualHandRank2 = HandCalculations.handRank(communityCards[2], hand[2]);

        // ASSERT
        assertEquals(expectedHandRank, actualHandRank0);
        assertEquals(expectedHandRank, actualHandRank1);
        assertEquals(expectedHandRank, actualHandRank2);
    }

    @Test
    public void winnerFoundWithBetterHandRank() {
        // ARRANGE
        CommunityCards communityCards = new CommunityCards();

        communityCards.addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards.addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards.addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards.addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards.addCard(new Card(Suit.HEART, Rank.TEN));

        // Nothing
        Hand expectedLosingHand = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );
        // Pair
        Hand expectedWinningHand = new Hand(
            new Card(Suit.SPADE, Rank.SEVEN),
            new Card(Suit.HEART, Rank.TWO)
        );

        // ACT
        Set<Integer> actualWinningIndices = HandCalculations.decideWinner(
            communityCards,
            expectedWinningHand,
            expectedLosingHand
        );

        // ASSERT
        assertTrue(actualWinningIndices.contains(0));
        assertEquals(1, actualWinningIndices.size());
    }

    @Test
    public void winnerFoundWithHighestCard() {
        // ARRANGE
        CommunityCards communityCards = new CommunityCards();

        communityCards.addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards.addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards.addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards.addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards.addCard(new Card(Suit.HEART, Rank.TWO));

        // Three of a kind, twos.
        Hand expectedLosingHand = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.DIAMOND, Rank.TWO)
        );
        // Three of a kind, sevens.
        Hand expectedWinningHand = new Hand(
            new Card(Suit.SPADE, Rank.SEVEN),
            new Card(Suit.HEART, Rank.SEVEN)
        );

        // ACT
        Set<Integer> actualWinningIndices = HandCalculations.decideWinner(
            communityCards,
            expectedLosingHand,
            expectedWinningHand
        );

        // ASSERT
        assertTrue(actualWinningIndices.contains(1));
        assertEquals(1, actualWinningIndices.size());
    }

    @Test
    public void multiplePeopleWinsWithSameHandRank() {
        // ARRANGE
        CommunityCards communityCards = new CommunityCards();

        communityCards.addCard(new Card(Suit.CLUB, Rank.ACE));
        communityCards.addCard(new Card(Suit.CLUB, Rank.FIVE));
        communityCards.addCard(new Card(Suit.DIAMOND, Rank.EIGHT));
        communityCards.addCard(new Card(Suit.CLUB, Rank.SEVEN));
        communityCards.addCard(new Card(Suit.HEART, Rank.KING));

        // Nothing, all highest cards are in the community cards.
        Hand expectedLosingHand = new Hand(
            new Card(Suit.HEART, Rank.TWO),
            new Card(Suit.HEART, Rank.THREE)
        );
        // Nothing, all highest cards are in the community cards.
        Hand expectedWinningHand = new Hand(
            new Card(Suit.SPADE, Rank.TWO),
            new Card(Suit.SPADE, Rank.THREE)
        );

        // ACT
        Set<Integer> actualWinningIndices = HandCalculations.decideWinner(
            communityCards,
            expectedLosingHand,
            expectedWinningHand
        );

        // ASSERT
        assertTrue(actualWinningIndices.contains(0));
        assertTrue(actualWinningIndices.contains(1));
        assertEquals(2, actualWinningIndices.size());
    }
}
